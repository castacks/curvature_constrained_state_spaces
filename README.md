# README #

This is a collection of state spaces (following ompl conventions) that deal with constraints on curvature and curvature rates.

### How do I get setup? ###

To see dubins z path, 


```
#!bash

rosrun curvature_constrained_state_spaces curvature_constrained_state_spaces_example_dubins_z_path
```
And use the rviz config example_dubins_z_path.rviz

### Author ###

* Sanjiban Choudhury (sanjiban@cmu.edu)