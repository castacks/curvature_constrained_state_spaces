/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * clothoid_state_space.cpp
 *
 *  Created on: Nov 30, 2014
 *      Author: Sanjiban Choudhury
 */

#include "curvature_constrained_state_spaces/clothoid_state_space.h"
#include "ompl/base/SpaceInformation.h"
#include "ompl/util/Exception.h"
#include <queue>
#include <boost/math/constants/constants.hpp>
#include <angles/angles.h>
#include "ompl/base/spaces/DubinsStateSpace.h"
#include "ompl/base/ScopedState.h"
namespace ob = ompl::base;

namespace ca {

double ClothoidStateSpace::distance(const ob::State *state1, const ob::State *state2) const {
  const StateType *s1 = static_cast<const StateType*>(state1);
  const StateType *s2 = static_cast<const StateType*>(state2);
  CurvatureBVPSolver::BVP start, end;
  start.x =  s1->GetPose().getX(); start.y =  s1->GetPose().getY(); start.psi = s1->GetPose().getYaw(); start.curvature = s1->GetCurvature();
  end.x =  s2->GetPose().getX(); end.y =  s2->GetPose().getY(); end.psi = s2->GetPose().getYaw(); end.curvature = s2->GetCurvature();

  if((start.x - end.x)*(start.x - end.x) + (start.y - end.y)*(start.y - end.y) + (start.psi - end.psi)*(start.psi - end.psi) +
      (start.curvature - end.curvature)*(start.curvature - end.curvature) < std::numeric_limits<double>::epsilon())
    return 0;

  CurvatureBVPSolver::Trajectory traj;
  bool result = bvp_solver_.SolveBVP(start, end, traj);
  //std::cout<<start.x<<" "<<start.y<<" "<<end.x<<" "<<end.y<<" res: "<<result<<"\n";
  if (result)
    return std::max(CurvatureBVPSolver::Length(traj), sqrt((start.x - end.x)*(start.x - end.x) + (start.y - end.y)*(start.y - end.y)));
  else
    return 0.6*std::numeric_limits<double>::max();
}

double ClothoidStateSpace::trueDistance(const ob::State *state1, const ob::State *state2) const {
  return distance(state1, state2);
}

double ClothoidStateSpace::heuristicDistance(const ompl::base::State *state1, const ompl::base::State *state2) const {
  const StateType *s1 = static_cast<const StateType*>(state1);
  const StateType *s2 = static_cast<const StateType*>(state2);
/*
  return sqrt((s1->GetPose().getX() - s2->GetPose().getX())*(s1->GetPose().getX() - s2->GetPose().getX()) +
              (s1->GetPose().getY() - s2->GetPose().getY())*(s1->GetPose().getY() - s2->GetPose().getY()));
*/

  ob::StateSpacePtr space(new ob::DubinsStateSpace(1/curvature_max_, false));
  ob::ScopedState<ob::DubinsStateSpace> state1_dub(space), state2_dub(space);

  state1_dub->setXY(s1->GetPose().getX(), s1->GetPose().getY());
  state1_dub->setYaw(s1->GetPose().getYaw());

  state2_dub->setXY(s2->GetPose().getX(), s2->GetPose().getY());
  state2_dub->setYaw(s2->GetPose().getYaw());

  return space->distance(state1_dub.get(), state2_dub.get())*0.9;
}


void ClothoidStateSpace::interpolate(const ob::State *from, const ob::State *to, const double t, ob::State *state) const {
  bool firstTime = true;
  CurvatureBVPSolver::Trajectory path;
  interpolate(from, to, t, firstTime, path, state);
}

bool ClothoidStateSpace::interpolate(const ob::State *from, const ob::State *to, const double t,
                                     bool& firstTime, CurvatureBVPSolver::Trajectory& path, ob::State *state) const {
  if (firstTime) {
    if (t>=1.) {
      if (to != state)
        copyState(state, to);
      return true;
    }
    if (t<=0.) {
      if (from != state)
        copyState(state, from);
      return true;
    }

    const StateType *s1 = static_cast<const StateType*>(from);
    const StateType *s2 = static_cast<const StateType*>(to);
    CurvatureBVPSolver::BVP start, end;
    start.x =  s1->GetPose().getX(); start.y =  s1->GetPose().getY(); start.psi = s1->GetPose().getYaw(); start.curvature = s1->GetCurvature();
    end.x =  s2->GetPose().getX(); end.y =  s2->GetPose().getY(); end.psi = s2->GetPose().getYaw(); end.curvature = s2->GetCurvature();

    bool result = bvp_solver_.SolveBVP(start, end, path);
    if (!result)
      return false;
    firstTime = false;
  }
  interpolate(path, t, state);
  return true;
}

bool ClothoidStateSpace::interpolate(const CurvatureBVPSolver::Trajectory& path, double t, ob::State *state) const {
  if (path.size() == 0)
    return false;

  double seg = t * CurvatureBVPSolver::Length(path);

  std::pair<Eigen::Vector3d, double> e = bvp_solver_.InterpolateCurv(path, seg);

  state->as<StateType>()->GetPose().setXY(e.first[0], e.first[1]);
  state->as<StateType>()->GetPose().setYaw(angles::normalize_angle(e.first[2]));
  state->as<StateType>()->SetCurvature(e.second);
  return true;
}



void ClothoidMotionValidator::DefaultSettings(void) {
  stateSpace_ = dynamic_cast<ClothoidStateSpace*>(si_->getStateSpace().get());
  if (!stateSpace_)
    throw ompl::Exception("No state space for motion validator");
}

bool ClothoidMotionValidator::checkMotion(const ob::State *s1, const ob::State *s2, std::pair<ob::State*, double> &lastValid) const {
  /* assume motion starts in a valid configuration so s1 is valid */
  bool result = true, firstTime = true, solved = true;
  CurvatureBVPSolver::Trajectory path;
  int nd = stateSpace_->validSegmentCount(s1, s2);

  if (nd > 1) {
    /* temporary storage for the checked state */
    ob::State *test = si_->allocState();

    for (int j = 1 ; j < nd ; ++j)
    {
      solved = stateSpace_->interpolate(s1, s2, (double)j / (double)nd, firstTime, path, test);
      if (!solved) {
        result = false;
        break;
      }
      if (!si_->isValid(test))
      {
        lastValid.second = (double)(j - 1) / (double)nd;
        if (lastValid.first)
          solved &= stateSpace_->interpolate(s1, s2, lastValid.second, firstTime, path, lastValid.first);
        result = false;
        break;
      }
    }
    si_->freeState(test);
  }

  if (result && solved)
    if (!si_->isValid(s2))
    {
      lastValid.second = (double)(nd - 1) / (double)nd;
      if (lastValid.first)
        solved &= stateSpace_->interpolate(s1, s2, lastValid.second, firstTime, path, lastValid.first);
      result = false;
    }

  if (result)
    valid_++;
  else
    invalid_++;

  if(!solved) {
    stateSpace_->copyState(lastValid.first, s1);
    lastValid.second = 0;
  }

  return result;
}

bool ClothoidMotionValidator::checkMotion(const ob::State *s1, const ob::State *s2) const {
  /* assume motion starts in a valid configuration so s1 is valid */
  if (!si_->isValid(s2))
    return false;

  bool result = true, firstTime = true, solved = true;
  CurvatureBVPSolver::Trajectory path;
  int nd = stateSpace_->validSegmentCount(s1, s2);

  /* initialize the queue of test positions */
  std::queue< std::pair<int, int> > pos;
  if (nd >= 2)
  {
    pos.push(std::make_pair(1, nd - 1));

    /* temporary storage for the checked state */
    ob::State *test = si_->allocState();

    /* repeatedly subdivide the path segment in the middle (and check the middle) */
    while (!pos.empty())
    {
      std::pair<int, int> x = pos.front();

      int mid = (x.first + x.second) / 2;
      solved = stateSpace_->interpolate(s1, s2, (double)mid / (double)nd, firstTime, path, test);

      if (!solved || !si_->isValid(test))
      {
        result = false;
        break;
      }

      pos.pop();

      if (x.first < mid)
        pos.push(std::make_pair(x.first, mid - 1));
      if (x.second > mid)
        pos.push(std::make_pair(mid + 1, x.second));
    }

    si_->freeState(test);
  }

  if (result)
    valid_++;
  else
    invalid_++;

  return result;
}


unsigned int ClothoidMotionValidator::getMotionStates(const ompl::base::State *s1, const ompl::base::State *s2, std::vector<ompl::base::State*> &states, unsigned int count, bool endpoints, bool alloc) const {
  // add 1 to the number of states we want to add between s1 & s2. This gives us the number of segments to split the motion into
  count++;
  CurvatureBVPSolver::Trajectory path;
  bool firstTime = true, solved = true;
  if (count < 2)
  {
    unsigned int added = 0;

    // if they want endpoints, then at most endpoints are included
    if (endpoints)
    {
      if (alloc)
      {
        states.resize(2);
        states[0] = si_->allocState();
        states[1] = si_->allocState();
      }
      if (states.size() > 0)
      {
        si_->copyState(states[0], s1);
        added++;
      }

      if (states.size() > 1)
      {
        si_->copyState(states[1], s2);
        added++;
      }
    }
    else
      if (alloc)
        states.resize(0);
    return added;
  }

  if (alloc)
  {
    states.resize(count + (endpoints ? 1 : -1));
    if (endpoints)
      states[0] = si_->allocState();
  }

  unsigned int added = 0;

  if (endpoints && states.size() > 0)
  {
    si_->copyState(states[0], s1);
    added++;
  }

  /* find the states in between */
  for (unsigned int j = 1 ; j < count && added < states.size() ; ++j)
  {
    if (alloc)
      states[added] = si_->allocState();
    solved = stateSpace_->interpolate(s1, s2, (double)j / (double)count, firstTime, path, states[added]);

    if (solved) {
      added++;
    } else {
      for (unsigned int l = 0; l <= added; l++)
        si_->freeState(states[l]);
      states.clear();
      added = 0;
      return added;
    }
  }

  if (added < states.size() && endpoints)
  {
    if (alloc)
      states[added] = si_->allocState();
    si_->copyState(states[added], s2);
    added++;
  }

  return added;

}


}


