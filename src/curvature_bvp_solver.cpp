/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * curvature_bvp_solver.cpp
 *
 *  Created on: Nov 29, 2014
 *      Author: Sanjiban Choudhury
 */

#include "curvature_constrained_state_spaces/curvature_bvp_solver.h"

#include <nlopt.hpp> // C++ interface for nlopt
#include <iostream>

#include "circular_curve_lib/dubins.h"
#include "tictoc_profiler/profiler.hpp"


namespace ca {

bool CurvatureBVPSolver::SolveBVP(const BVP& start, const BVP& end, Trajectory &traj) const{

  traj.clear();

  // Step 1. Solve analytic dubins
  double q0[3], q1[3];
  q0[0] = start.x;
  q0[1] = start.y;
  q0[2] = start.psi;

  q1[0] = end.x;
  q1[1] = end.y;
  q1[2] = end.psi;

  dubin::DubinPath path;
  int rval = dubin::dubin_init( q0, q1, 1/curv_max_, &path);
  if ( rval > 1 )
    return false;

  const int L_SEG (0);
  const int S_SEG (1);
  const int R_SEG (2);
  const int DIRDATA[][3] = {
      { L_SEG, S_SEG, L_SEG },
      { L_SEG, S_SEG, R_SEG },
      { R_SEG, S_SEG, L_SEG },
      { R_SEG, S_SEG, R_SEG },
      { R_SEG, L_SEG, R_SEG },
      { L_SEG, R_SEG, L_SEG }
  };
  const int *type = DIRDATA[path.type];


  // Step 2. Setup curvature
  std::vector<double> curv_param;
  curv_param.push_back(start.curvature);

  if (type[0] == 0)
    curv_param.push_back(curv_max_);
  else if (type[0] == 2 )
    curv_param.push_back(-curv_max_);

  if (type[1] == 0)
    curv_param.push_back(curv_max_);
  else if (type[1] == 1 )
    curv_param.push_back(0);
  else if (type[1] == 2 )
    curv_param.push_back(-curv_max_);

  if (type[2] == 0 )
    curv_param.push_back(curv_max_);
  else if (type[2] == 2 )
    curv_param.push_back(-curv_max_);

  curv_param.push_back(end.curvature);

  // Step 3. Initial guess
  std::vector<double> p0 {fabs(curv_param[0] -  curv_param[1])/curv_rate_max_,
    path.param[0]*path.rho,
    fabs(curv_param[1] -  curv_param[2])/curv_rate_max_,
    path.param[1]*path.rho,
    fabs(curv_param[2] -  curv_param[3])/curv_rate_max_,
    path.param[2]*path.rho,
    fabs(curv_param[3] -  curv_param[4])/curv_rate_max_};

  // Step 4. Lowerbound
  std::vector<double> lb {fabs(curv_param[0] -  curv_param[1])/curv_rate_max_,
    0.0,
    fabs(curv_param[1] -  curv_param[2])/curv_rate_max_,
    0.0,
    fabs(curv_param[2] -  curv_param[3])/curv_rate_max_,
    0.0,
    fabs(curv_param[3] -  curv_param[4])/curv_rate_max_};

  std::vector<double> ub {fabs(curv_param[0] -  curv_param[1])/curv_rate_max_ +200,
    path.param[0]*path.rho + 500,
    fabs(curv_param[1] -  curv_param[2])/curv_rate_max_+200,
    path.param[1]*path.rho + 500,
    fabs(curv_param[2] -  curv_param[3])/curv_rate_max_+200,
    path.param[2]*path.rho + 500,
    fabs(curv_param[3] -  curv_param[4])/curv_rate_max_+200};

  // Step 5. Setup optimization
  nlopt::opt opt(nlopt::LN_COBYLA, p0.size());

  double length_upper_bound = sqrt((start.x - end.x)*(start.x - end.x) + (start.y - end.y)*(start.y - end.y)) + 2*M_PI*1/curv_max_ + 2*curv_max_/curv_rate_max_;
  opt.set_lower_bounds(lb);
  opt.set_upper_bounds(2*length_upper_bound);
  // set objective function

  OptData data;
  data.x0 = Eigen::Vector3d(start.x, start.y, start.psi);
  data.xf = Eigen::Vector3d(end.x, end.y, end.psi);
  data.curv_param = curv_param;
  data.min_rad = 1/curv_max_;
  data.dist_threshold = options_.dist_threshold;
  data.dot_threshold = options_.dot_threshold;

  opt.set_min_objective(CostFunction, &data);
  std::vector<double> tol(8, 0);
  opt.add_inequality_mconstraint(ConstraintFunction, &data, tol);
  opt.set_maxtime(options_.computation_time);
  opt.set_xtol_abs(options_.parameter_tolerance);
  opt.set_ftol_rel(options_.rel_function_tolerance);

  double minf;
  try {
    nlopt::result result = opt.optimize(p0, minf);
    if (result == nlopt::SUCCESS || result == nlopt::FTOL_REACHED || result == nlopt::XTOL_REACHED) {
      Eigen::Vector3d x;
      std::vector<double> p = p0;
      double r_min = data.min_rad;
      Segment segment;
      x = data.x0;
      segment.offset = x;
      segment.type = Segment::CLOTHOID;
      segment.curv_set = std::vector<double> {curv_param[0], curv_param[1]};
      segment.distance = p[0];
      traj.push_back(segment);
      x = IntegrateClothoid(data.x0, curv_param[0], curv_param[1], p[0], p[0], 0.01*r_min);

      segment.offset = x;
      segment.type = Segment::ARC;
      segment.curv_set = std::vector<double> {curv_param[1]};
      segment.distance = p[1];
      traj.push_back(segment);
      x = IntegrateArc(x, curv_param[1], p[1], p[1]);


      if (fabs(curv_param[2]) <= std::numeric_limits<double>::epsilon()) {
        segment.offset = x;
        segment.type = Segment::CLOTHOID;
        segment.curv_set = std::vector<double> {curv_param[1], 0};
        segment.distance = p[2];
        traj.push_back(segment);
        x = IntegrateClothoid(x, curv_param[1], 0, p[2], p[2], 0.01*r_min);

        segment.offset = x;
        segment.type = Segment::STRAIGHT;
        segment.curv_set = std::vector<double> {0};
        segment.distance = p[3];
        traj.push_back(segment);
        x = IntegrateStraight(x, p[3],p[3]);

        segment.offset = x;
        segment.type = Segment::CLOTHOID;
        segment.curv_set = std::vector<double> {0, curv_param[3]};
        segment.distance = p[4];
        traj.push_back(segment);
        x = IntegrateClothoid(x, 0, curv_param[3], p[4],p[4], 0.01*r_min);
      } else {
        segment.offset = x;
        segment.type = Segment::CLOTHOID;
        segment.curv_set = std::vector<double> {curv_param[1], curv_param[2]};
        segment.distance = p[2];
        traj.push_back(segment);
        x = IntegrateClothoid(x, curv_param[1], curv_param[2], p[2], p[2], 0.01*r_min);

        segment.offset = x;
        segment.type = Segment::ARC;
        segment.curv_set = std::vector<double> {curv_param[2]};
        segment.distance = p[3];
        traj.push_back(segment);
        x = IntegrateArc(x, curv_param[2], p[3], p[3]);

        segment.offset = x;
        segment.type = Segment::CLOTHOID;
        segment.curv_set = std::vector<double> {curv_param[2], curv_param[3]};
        segment.distance = p[4];
        traj.push_back(segment);
        x = IntegrateClothoid(x, curv_param[2], curv_param[3], p[4], p[4], 0.01*r_min);
      }

      segment.offset = x;
      segment.type = Segment::ARC;
      segment.curv_set = std::vector<double> {curv_param[3]};
      segment.distance = p[5];
      traj.push_back(segment);
      x = IntegrateArc(x, curv_param[3], p[5], p[5]);

      segment.offset = x;
      segment.type = Segment::CLOTHOID;
      segment.curv_set = std::vector<double> {curv_param[3], curv_param[4]};
      segment.distance = p[6];
      traj.push_back(segment);
      x = IntegrateClothoid(x, curv_param[3], curv_param[4], p[6], p[6], 0.01*r_min);

      return true;
    } else {
      return false;
    }
  }  catch (...) {
    // catched exception from optimization --> return false
    return false;
  }
}

double CurvatureBVPSolver::Length(const Trajectory &traj) {
  double distance = 0;
  for (auto it : traj)
    distance += it.distance;
  return distance;
}

Eigen::Vector3d CurvatureBVPSolver::Interpolate(const Trajectory &traj, double s) const{
  if (s <=0 )
    return traj.front().offset;
  else if (s >= Length(traj))
    return traj.back().offset;

  double running_distance = 0;
  for (auto it : traj) {
    running_distance += it.distance;
    if (s < running_distance) {
      double seval = s - (running_distance - it.distance);
      if (it.type == Segment::CLOTHOID)
        return IntegrateClothoid(it.offset, it.curv_set[0], it.curv_set[1], it.distance, seval, 0.01*(1/curv_max_));
      else if (it.type == Segment::ARC)
        return IntegrateArc(it.offset, it.curv_set[0], it.distance, seval);
      else
        return IntegrateStraight(it.offset, it.distance, seval);
    }
  }
  return Eigen::Vector3d::Zero();
}

std::pair<Eigen::Vector3d, double> CurvatureBVPSolver::InterpolateCurv(const Trajectory &traj, double s) const{
  if (s <=0 )
    return std::make_pair(traj.front().offset, traj.front().curv_set.front());
  else if (s >= Length(traj))
    return std::make_pair(traj.back().offset, traj.back().curv_set.back());

  double running_distance = 0;
  for (auto it : traj) {
    running_distance += it.distance;
    if (s < running_distance) {
      double seval = s - (running_distance - it.distance);
      if (it.type == Segment::CLOTHOID)
        return std::make_pair(IntegrateClothoid(it.offset, it.curv_set[0], it.curv_set[1], it.distance, seval, 0.01*(1/curv_max_)),
                              it.curv_set[0] + (it.curv_set[1] - it.curv_set[0])*(seval/it.distance));
      else if (it.type == Segment::ARC)
        return std::make_pair(IntegrateArc(it.offset, it.curv_set[0], it.distance, seval), it.curv_set[0]);
      else
        return std::make_pair(IntegrateStraight(it.offset, it.distance, seval), 0);
    }
  }
  return std::make_pair(Eigen::Vector3d::Zero(), 0);
}


Eigen::Vector3d CurvatureBVPSolver::IntegrateClothoid(const Eigen::Vector3d &x0, double K1, double K2, double sf, double seval, double delta_s) {
  Eigen::Vector3d xf(x0);
  int num_wp = std::min(20, std::max(5, (int)std::ceil(seval/delta_s)));
  double ds = seval/(num_wp - 1);
  for (int i = 0; i < num_wp; i++) {
    double s = ((double)i/(double)num_wp)*seval;
    double psi = x0[2] + K1*s + ((K2-K1)/(2*(std::numeric_limits<double>::epsilon()+sf)))*s*s;
    xf[0] += cos(psi)*ds;
    xf[1] += sin(psi)*ds;
  }
  xf[2] = x0[2] + K1*seval + ((K2-K1)/(2*(std::numeric_limits<double>::epsilon()+sf)))*seval*seval;
  return xf;
}

Eigen::Vector3d CurvatureBVPSolver::IntegrateArc(const Eigen::Vector3d &x0, double K, double sf, double seval) {
  Eigen::Vector3d xf(0,0,0);
  xf[0] = x0[0] + (1/K)*(sin(x0[2] + K*seval) - sin(x0[2]));
  xf[1] = x0[1] - (1/K)*(cos(x0[2] + K*seval) - cos(x0[2]));
  xf[2] = x0[2] + K*seval;
  return xf;
}

Eigen::Vector3d CurvatureBVPSolver::IntegrateStraight(const Eigen::Vector3d &x0, double sf, double seval) {
  Eigen::Vector3d xf(0,0,0);
  xf[0] = x0[0] + seval*cos(x0[2]);
  xf[1] = x0[1] + seval*sin(x0[2]);
  xf[2] = x0[2];
  return xf;
}

double CurvatureBVPSolver::CostFunction(const std::vector<double> &p, std::vector<double> &grad, void *f_data) {
  //  OptData *data = static_cast<OptData*> (f_data);
  double distance = 0;
  for (auto it : p)
    distance += it;

  //  Eigen::Vector3d x0(data->x0);
  //  Eigen::Vector3d xf(data->xf);
  //  std::vector<double> curv_param(data->curv_param);
  //  double r_min = data->min_rad;
  //
  //
  //
  //  Eigen::Vector3d x;
  //  x = IntegrateClothoid(x0, curv_param[0], curv_param[1], p[0], 0.1*r_min);
  //  x = IntegrateArc(x, curv_param[1], p[1]);
  //  if (fabs(curv_param[2]) <= std::numeric_limits<double>::epsilon()) {
  //    x = IntegrateClothoid(x, curv_param[1], 0, p[2], 0.1*r_min);
  //    x = IntegrateStraight(x, p[3]);
  //    x = IntegrateClothoid(x, 0, curv_param[3], p[4], 0.1*r_min);
  //  } else {
  //    x = IntegrateClothoid(x, curv_param[1], curv_param[2], p[2], 0.1*r_min);
  //    x = IntegrateArc(x, curv_param[2], p[3]);
  //    x = IntegrateClothoid(x, curv_param[2], curv_param[3], p[4], 0.1*r_min);
  //  }
  //  x = IntegrateArc(x, curv_param[3], p[5]);
  //  x = IntegrateClothoid(x, curv_param[3], curv_param[4], p[6], 0.1*r_min);
  //
  ////  std::cout<<"p ";
  ////  for (auto it : p)
  ////    std::cout<<it<<" ";
  ////  std::cout<<"\n";
  ////  std::cout <<" x "<<x.transpose()<<"\n";
  ////  std::cout <<" xf "<<xf.transpose()<<"\n";
  //
  //  double cost = distance + 1e4*((xf[0] - x[0])*(xf[0] - x[0]) + (xf[1] - x[1])*(xf[1] - x[1]))
  //                         + 1e4*(1 - cos(xf[2])*cos(x[2]) - sin(xf[2])*sin(x[2]) );
  //  //std::cout <<"Cost "<<cost<<"\n";
  //  //sleep(1);
  //  return cost;

  return distance;
}

void CurvatureBVPSolver::ConstraintFunction(unsigned m, double *result, unsigned n, const double* p, double* grad, void* f_data) {
  OptData *data = static_cast<OptData*> (f_data);

  Eigen::Vector3d x0(data->x0);
  Eigen::Vector3d xf(data->xf);
  std::vector<double> curv_param(data->curv_param);
  double r_min = data->min_rad;

  Eigen::Vector3d x;
  x = IntegrateClothoid(x0, curv_param[0], curv_param[1], p[0], p[0], 0.01*r_min);
  x = IntegrateArc(x, curv_param[1], p[1], p[1]);
  if (fabs(curv_param[2]) <= std::numeric_limits<double>::epsilon()) {
    x = IntegrateClothoid(x, curv_param[1], 0, p[2], p[2], 0.01*r_min);
    x = IntegrateStraight(x, p[3], p[3]);
    x = IntegrateClothoid(x, 0, curv_param[3], p[4], p[4], 0.01*r_min);
  } else {
    x = IntegrateClothoid(x, curv_param[1], curv_param[2], p[2],  p[2], 0.01*r_min);
    x = IntegrateArc(x, curv_param[2], p[3], p[3]);
    x = IntegrateClothoid(x, curv_param[2], curv_param[3], p[4], p[4], 0.01*r_min);
  }
  x = IntegrateArc(x, curv_param[3], p[5], p[5]);
  x = IntegrateClothoid(x, curv_param[3], curv_param[4], p[6],  p[6], 0.01*r_min);

  result[0] = x[0] - xf[0] - data->dist_threshold;
  result[1] = xf[0] - x[0] - data->dist_threshold;
  result[2] = x[1] - xf[1] - data->dist_threshold;
  result[3] = xf[1] - x[1] - data->dist_threshold;
  result[4] = cos(x[2]) - cos(xf[2]) - data->dot_threshold;
  result[5] = cos(xf[2]) - cos(x[2]) - data->dot_threshold;
  result[6] = sin(x[2]) - sin(xf[2]) - data->dot_threshold;
  result[7] = sin(xf[2]) - sin(x[2]) - data->dot_threshold;
}


}
