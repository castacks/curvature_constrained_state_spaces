/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * dubins_feasible_path_generator.cpp
 *
 *  Created on: Jan 31, 2015
 *      Author: Sanjiban Choudhury
 */

#include "curvature_constrained_state_spaces/dubins_feasible_path_generator.h"
#include "ompl/base/spaces/DubinsStateSpace.h"
#include "ompl/base/ScopedState.h"
#include "circular_curve_lib/dubins.h"

namespace ob = ompl::base;

namespace ca {

bool DubinsFeasiblePathGenerator::GetFeasiblePath(const ompl::base::State *state1, const ompl::base::State *state2, planning_common::PathWaypoint &feasible_path, double resolution) {
  // Cast to Dubins state space type
  const ob::DubinsStateSpace::StateType *start = static_cast<const ob::DubinsStateSpace::StateType*>(state1);
  const ob::DubinsStateSpace::StateType *goal = static_cast<const ob::DubinsStateSpace::StateType*>(state2);

  // Check bvps to eliminate waste of time
  if(!si_->isValid(start))
    return false;
  if(!si_->isValid(goal))
    return false;

  // Get feasible path set
  double q0[3] = {start->getX(), start->getY(), start->getYaw()};
  double q1[3] = {goal->getX(),  goal->getY(),  goal->getYaw()};
  double radius = si_->getStateSpace()->as<ob::DubinsStateSpace>()->rho();
  std::vector<dubin::DubinPath> feasible_candidates;
  if (dubin::dubin_init_pathset(q0, q1, radius, feasible_candidates)!=0)
    return false;

  bool found_path = true;
  for (auto candidate : feasible_candidates) {
    found_path = true;
    double length = dubin::dubin_path_length(&candidate);
    for (double t = 0; t < length; t+= resolution) {
      double q[3];
      dubin::dubin_path_sample(&candidate, t, q);
      ob::ScopedState<ob::DubinsStateSpace> sample(si_);
      sample->setXY(q[0], q[1]);
      sample->setYaw(q[2]);
      if (si_->isValid(sample.get())) {
        feasible_path.AppendWaypoint(sample.get());
      } else {
        found_path = false;
        break;
      }
    }
    if (found_path) {
      feasible_path.AppendWaypoint(goal);
      break;
    } else {
      feasible_path.Clear();
    }
  }

  return found_path;
}


}


