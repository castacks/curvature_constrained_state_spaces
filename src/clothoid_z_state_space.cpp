/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * clothoid_z_state_space.cpp
 *
 *  Created on: Mar 15, 2015
 *      Author: Sanjiban Choudhury
 */

#include "curvature_constrained_state_spaces/clothoid_z_state_space.h"
#include "ompl/base/SpaceInformation.h"
#include "ompl/util/Exception.h"
#include <queue>
#include <boost/math/constants/constants.hpp>
#include <angles/angles.h>
#include "ompl/base/ScopedState.h"
namespace ob = ompl::base;

namespace ca {

double ClothoidZStateSpace::distance(const ob::State *state1, const ob::State *state2) const {
  const StateType *s1 = static_cast<const StateType*>(state1);
  const StateType *s2 = static_cast<const StateType*>(state2);

  ob::State *clothoid_s1 = clothoid_space_->allocState();
  ob::State *clothoid_s2 = clothoid_space_->allocState();

  clothoid_s1->as<ca::ClothoidStateSpace::StateType>()->GetPose().setXY(s1->GetTranslation().values[0], s1->GetTranslation().values[1]);
  clothoid_s1->as<ca::ClothoidStateSpace::StateType>()->GetPose().setYaw(s1->GetRotation().value);
  clothoid_s1->as<ca::ClothoidStateSpace::StateType>()->SetCurvature(s1->GetCurvature());

  clothoid_s2->as<ca::ClothoidStateSpace::StateType>()->GetPose().setXY(s2->GetTranslation().values[0], s2->GetTranslation().values[1]);
  clothoid_s2->as<ca::ClothoidStateSpace::StateType>()->GetPose().setYaw(s2->GetRotation().value);
  clothoid_s2->as<ca::ClothoidStateSpace::StateType>()->SetCurvature(s2->GetCurvature());

  double xydist = clothoid_space_->distance(clothoid_s1, clothoid_s2);
  double zdist = s2->GetTranslation().values[2] - s1->GetTranslation().values[2];

  clothoid_space_->freeState(clothoid_s1);
  clothoid_space_->freeState(clothoid_s2);

  if (xydist > 0.5*std::numeric_limits<double>::max() || abs(zdist) > max_slope_*xydist)
    return 0.6*std::numeric_limits<double>::max();
  else
    return sqrt(xydist*xydist + zdist*zdist);
}

double ClothoidZStateSpace::trueDistance(const ob::State *state1, const ob::State *state2) const {
  return distance(state1, state2);
}

double ClothoidZStateSpace::heuristicDistance(const ompl::base::State *state1, const ompl::base::State *state2) const {
  const StateType *s1 = static_cast<const StateType*>(state1);
  const StateType *s2 = static_cast<const StateType*>(state2);

  ob::State *clothoid_s1 = clothoid_space_->allocState();
  ob::State *clothoid_s2 = clothoid_space_->allocState();

  clothoid_s1->as<ca::ClothoidStateSpace::StateType>()->GetPose().setXY(s1->GetTranslation().values[0], s1->GetTranslation().values[1]);
  clothoid_s1->as<ca::ClothoidStateSpace::StateType>()->GetPose().setYaw(s1->GetRotation().value);
  clothoid_s1->as<ca::ClothoidStateSpace::StateType>()->SetCurvature(s1->GetCurvature());

  clothoid_s2->as<ca::ClothoidStateSpace::StateType>()->GetPose().setXY(s2->GetTranslation().values[0], s2->GetTranslation().values[1]);
  clothoid_s2->as<ca::ClothoidStateSpace::StateType>()->GetPose().setYaw(s2->GetRotation().value);
  clothoid_s2->as<ca::ClothoidStateSpace::StateType>()->SetCurvature(s2->GetCurvature());

  double xydist = clothoid_space_->heuristicDistance(clothoid_s1, clothoid_s2);
  double zdist = s2->GetTranslation().values[2] - s1->GetTranslation().values[2];

  clothoid_space_->freeState(clothoid_s1);
  clothoid_space_->freeState(clothoid_s2);
  return sqrt(xydist*xydist + zdist*zdist);
}


void ClothoidZStateSpace::interpolate(const ob::State *from, const ob::State *to, const double t, ob::State *state) const {
  bool firstTime = true;
  CurvatureBVPSolver::Trajectory path;
  interpolate(from, to, t, firstTime, path, state);
}

void ClothoidZStateSpace::interpolate(const ob::State *from, const ob::State *to, const double t,
                                     bool& firstTime, CurvatureBVPSolver::Trajectory& path, ob::State *state) const {
  const StateType *s1 = static_cast<const StateType*>(from);
  const StateType *s2 = static_cast<const StateType*>(to);

  ob::State *clothoid_s1 = clothoid_space_->allocState();
  ob::State *clothoid_s2 = clothoid_space_->allocState();

  clothoid_s1->as<ca::ClothoidStateSpace::StateType>()->GetPose().setXY(s1->GetTranslation().values[0], s1->GetTranslation().values[1]);
  clothoid_s1->as<ca::ClothoidStateSpace::StateType>()->GetPose().setYaw(s1->GetRotation().value);
  clothoid_s1->as<ca::ClothoidStateSpace::StateType>()->SetCurvature(s1->GetCurvature());

  clothoid_s2->as<ca::ClothoidStateSpace::StateType>()->GetPose().setXY(s2->GetTranslation().values[0], s2->GetTranslation().values[1]);
  clothoid_s2->as<ca::ClothoidStateSpace::StateType>()->GetPose().setYaw(s2->GetRotation().value);
  clothoid_s2->as<ca::ClothoidStateSpace::StateType>()->SetCurvature(s2->GetCurvature());

  ob::State *clothoid_res = clothoid_space_->allocState();
  clothoid_space_->interpolate(clothoid_s1, clothoid_s2, t, firstTime, path, clothoid_res);
  state->as<StateType>()->GetTranslation().values[0] = clothoid_res->as<ca::ClothoidStateSpace::StateType>()->GetPose().getX();
  state->as<StateType>()->GetTranslation().values[1] = clothoid_res->as<ca::ClothoidStateSpace::StateType>()->GetPose().getY();
  state->as<StateType>()->GetRotation().value = clothoid_res->as<ca::ClothoidStateSpace::StateType>()->GetPose().getYaw();
  state->as<StateType>()->GetTranslation().values[2] = ((1-t)*s1->GetTranslation().values[2] + t*s2->GetTranslation().values[2]);
  clothoid_space_->freeState(clothoid_s1);
  clothoid_space_->freeState(clothoid_s2);
  clothoid_space_->freeState(clothoid_res);
}



void ClothoidZMotionValidator::DefaultSettings(void) {
  stateSpace_ = dynamic_cast<ClothoidZStateSpace*>(si_->getStateSpace().get());
  if (!stateSpace_)
    throw ompl::Exception("No state space for motion validator");
}

bool ClothoidZMotionValidator::checkMotion(const ob::State *s1, const ob::State *s2, std::pair<ob::State*, double> &lastValid) const {
  /* assume motion starts in a valid configuration so s1 is valid */
  bool result = true, firstTime = true, solved = true;
  ca::CurvatureBVPSolver::Trajectory path;
  int nd = stateSpace_->validSegmentCount(s1, s2);

  if (stateSpace_->distance(s1, s2) > 0.5*std::numeric_limits<double>::max()){
    stateSpace_->copyState(lastValid.first, s1);
    lastValid.second = 0;
    return false;
  }

  if (nd > 1) {
    /* temporary storage for the checked state */
    ob::State *test = si_->allocState();

    for (int j = 1 ; j < nd ; ++j)
    {
      stateSpace_->interpolate(s1, s2, (double)j / (double)nd, firstTime, path, test);
      if (!solved) {
        result = false;
        break;
      }
      if (!si_->isValid(test))
      {
        lastValid.second = (double)(j - 1) / (double)nd;
        if (lastValid.first)
          stateSpace_->interpolate(s1, s2, lastValid.second, firstTime, path, lastValid.first);
        result = false;
        break;
      }
    }
    si_->freeState(test);
  }

  if (result && solved)
    if (!si_->isValid(s2))
    {
      lastValid.second = (double)(nd - 1) / (double)nd;
      if (lastValid.first)
        stateSpace_->interpolate(s1, s2, lastValid.second, firstTime, path, lastValid.first);
      result = false;
    }

  if (result)
    valid_++;
  else
    invalid_++;

  if(!solved) {
    stateSpace_->copyState(lastValid.first, s1);
    lastValid.second = 0;
  }

  return result;
}

bool ClothoidZMotionValidator::checkMotion(const ob::State *s1, const ob::State *s2) const {
  /* assume motion starts in a valid configuration so s1 is valid */
  if (!si_->isValid(s2))
    return false;

  bool result = true, firstTime = true, solved = true;
  ca::CurvatureBVPSolver::Trajectory path;
  int nd = stateSpace_->validSegmentCount(s1, s2);

  if (stateSpace_->distance(s1, s2) > 0.5*std::numeric_limits<double>::max())
    return false;


  /* initialize the queue of test positions */
  std::queue< std::pair<int, int> > pos;
  if (nd >= 2)
  {
    pos.push(std::make_pair(1, nd - 1));

    /* temporary storage for the checked state */
    ob::State *test = si_->allocState();

    /* repeatedly subdivide the path segment in the middle (and check the middle) */
    while (!pos.empty())
    {
      std::pair<int, int> x = pos.front();

      int mid = (x.first + x.second) / 2;
      stateSpace_->interpolate(s1, s2, (double)mid / (double)nd, firstTime, path, test);

      if (!solved || !si_->isValid(test))
      {
        result = false;
        break;
      }

      pos.pop();

      if (x.first < mid)
        pos.push(std::make_pair(x.first, mid - 1));
      if (x.second > mid)
        pos.push(std::make_pair(mid + 1, x.second));
    }

    si_->freeState(test);
  }

  if (result)
    valid_++;
  else
    invalid_++;

  return result;
}


unsigned int ClothoidZMotionValidator::getMotionStates(const ompl::base::State *s1, const ompl::base::State *s2, std::vector<ompl::base::State*> &states, unsigned int count, bool endpoints, bool alloc) const {
  // add 1 to the number of states we want to add between s1 & s2. This gives us the number of segments to split the motion into
  count++;
  CurvatureBVPSolver::Trajectory path;
  bool firstTime = true, solved = true;
  if (count < 2)
  {
    unsigned int added = 0;

    // if they want endpoints, then at most endpoints are included
    if (endpoints)
    {
      if (alloc)
      {
        states.resize(2);
        states[0] = si_->allocState();
        states[1] = si_->allocState();
      }
      if (states.size() > 0)
      {
        si_->copyState(states[0], s1);
        added++;
      }

      if (states.size() > 1)
      {
        si_->copyState(states[1], s2);
        added++;
      }
    }
    else
      if (alloc)
        states.resize(0);
    return added;
  }

  if (alloc)
  {
    states.resize(count + (endpoints ? 1 : -1));
    if (endpoints)
      states[0] = si_->allocState();
  }

  unsigned int added = 0;

  if (endpoints && states.size() > 0)
  {
    si_->copyState(states[0], s1);
    added++;
  }

  /* find the states in between */
  for (unsigned int j = 1 ; j < count && added < states.size() ; ++j)
  {
    if (alloc)
      states[added] = si_->allocState();
   stateSpace_->interpolate(s1, s2, (double)j / (double)count, firstTime, path, states[added]);

    if (solved) {
      added++;
    } else {
      for (unsigned int l = 0; l <= added; l++)
        si_->freeState(states[l]);
      states.clear();
      added = 0;
      return added;
    }
  }

  if (added < states.size() && endpoints)
  {
    if (alloc)
      states[added] = si_->allocState();
    si_->copyState(states[added], s2);
    added++;
  }

  return added;

}


}



