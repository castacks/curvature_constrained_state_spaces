/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * dubins_z_state_space.cpp
 *
 *  Created on: Mar 14, 2015
 *      Author: Sanjiban Choudhury
 */

#include "curvature_constrained_state_spaces/dubins_z_state_space.h"
#include "ompl/base/SpaceInformation.h"
#include "ompl/util/Exception.h"
#include <queue>
#include <boost/math/constants/constants.hpp>
#include <angles/angles.h>
#include "ompl/base/spaces/DubinsStateSpace.h"
#include "ompl/base/ScopedState.h"
#include "ompl/tools/config/MagicConstants.h"

namespace ob = ompl::base;
namespace pc = ca::planning_common;

namespace ca {

double DubinsZStateSpace::distance(const ob::State *state1, const ob::State *state2) const {
  const StateType *s1 = static_cast<const StateType*>(state1);
  const StateType *s2 = static_cast<const StateType*>(state2);

  ob::State *dub_s1 = dubins_space_->allocState();
  ob::State *dub_s2 = dubins_space_->allocState();

  dub_s1->as<ob::DubinsStateSpace::StateType>()->setXY(s1->GetTranslation().values[0], s1->GetTranslation().values[1]);
  dub_s1->as<ob::DubinsStateSpace::StateType>()->setYaw(s1->GetRotation().value);

  dub_s2->as<ob::DubinsStateSpace::StateType>()->setXY(s2->GetTranslation().values[0], s2->GetTranslation().values[1]);
  dub_s2->as<ob::DubinsStateSpace::StateType>()->setYaw(s2->GetRotation().value);

  double xydist = dubins_space_->distance(dub_s1, dub_s2);
  double zdist = s2->GetTranslation().values[2] - s1->GetTranslation().values[2];

  dubins_space_->freeState(dub_s1);
  dubins_space_->freeState(dub_s2);
  if ((zdist > 0 && zdist > max_slope_pos_z_*xydist) ||
      (zdist < 0 && zdist < -max_slope_neg_z_*xydist))
    return 0.6*std::numeric_limits<double>::max();
  else
    return sqrt(xydist*xydist + zdist*zdist);
}

double DubinsZStateSpace::trueDistance(const ob::State *state1, const ob::State *state2) const {
  return distance(state1, state2);
}

double DubinsZStateSpace::heuristicDistance(const ompl::base::State *state1, const ompl::base::State *state2) const {
  const StateType *s1 = static_cast<const StateType*>(state1);
  const StateType *s2 = static_cast<const StateType*>(state2);
/*
  ob::State *dub_s1 = dubins_space_->allocState();
  ob::State *dub_s2 = dubins_space_->allocState();

  dub_s1->as<ob::DubinsStateSpace::StateType>()->setXY(s1->GetTranslation().values[0], s1->GetTranslation().values[1]);
  dub_s1->as<ob::DubinsStateSpace::StateType>()->setYaw(s1->GetRotation().value);

  dub_s2->as<ob::DubinsStateSpace::StateType>()->setXY(s2->GetTranslation().values[0], s2->GetTranslation().values[1]);
  dub_s2->as<ob::DubinsStateSpace::StateType>()->setYaw(s2->GetRotation().value);

  double xydist = dubins_space_->distance(dub_s1, dub_s2);
  double zdist = s2->GetTranslation().values[2] - s1->GetTranslation().values[2];

  dubins_space_->freeState(dub_s1);
  dubins_space_->freeState(dub_s2);
  return sqrt(xydist*xydist + zdist*zdist);*/
  return std::sqrt((s1->GetTranslation().values[0] - s2->GetTranslation().values[0])*(s1->GetTranslation().values[0] - s2->GetTranslation().values[0]) +
      (s1->GetTranslation().values[1] - s2->GetTranslation().values[1])*(s1->GetTranslation().values[1] - s2->GetTranslation().values[1]) +
      (s1->GetTranslation().values[2] - s2->GetTranslation().values[2])*(s1->GetTranslation().values[2] - s2->GetTranslation().values[2]));

}


void DubinsZStateSpace::interpolate(const ob::State *from, const ob::State *to, const double t, ob::State *state) const {
  bool firstTime = true;
  ob::DubinsStateSpace::DubinsPath path;
  interpolate(from, to, t, firstTime, path, state);
}

void DubinsZStateSpace::interpolate(const ob::State *from, const ob::State *to, const double t,
                                    bool& firstTime, ompl::base::DubinsStateSpace::DubinsPath& path, ob::State *state) const {
  const StateType *s1 = static_cast<const StateType*>(from);
  const StateType *s2 = static_cast<const StateType*>(to);

  ob::State *dub_s1 = dubins_space_->allocState();
  ob::State *dub_s2 = dubins_space_->allocState();

  dub_s1->as<ob::DubinsStateSpace::StateType>()->setXY(s1->GetTranslation().values[0], s1->GetTranslation().values[1]);
  dub_s1->as<ob::DubinsStateSpace::StateType>()->setYaw(s1->GetRotation().value);

  dub_s2->as<ob::DubinsStateSpace::StateType>()->setXY(s2->GetTranslation().values[0], s2->GetTranslation().values[1]);
  dub_s2->as<ob::DubinsStateSpace::StateType>()->setYaw(s2->GetRotation().value);

  ob::State *dub_res = dubins_space_->allocState();
  dubins_space_->interpolate(dub_s1, dub_s2, t, firstTime, path, dub_res);
  state->as<StateType>()->GetTranslation().values[0] = dub_res->as<ob::DubinsStateSpace::StateType>()->getX();
  state->as<StateType>()->GetTranslation().values[1] = dub_res->as<ob::DubinsStateSpace::StateType>()->getY();
  state->as<StateType>()->GetRotation().value = dub_res->as<ob::DubinsStateSpace::StateType>()->getYaw();
  state->as<StateType>()->GetTranslation().values[2] = ((1-t)*s1->GetTranslation().values[2] + t*s2->GetTranslation().values[2]);
  dubins_space_->freeState(dub_s1);
  dubins_space_->freeState(dub_s2);
  dubins_space_->freeState(dub_res);
}

void DubinsZStateSpace::registerProjections(void)
{
    class DubinsZDefaultProjection : public ob::ProjectionEvaluator
    {
    public:

      DubinsZDefaultProjection(const ob::StateSpace *space) : ob::ProjectionEvaluator(space)
        {
        }

        virtual unsigned int getDimension(void) const
        {
            return 3;
        }

        virtual void defaultCellSizes(void)
        {
            cellSizes_.resize(3);
            bounds_ = space_->as<DubinsZStateSpace>()->getBounds();
            cellSizes_[0] = (bounds_.high[0] - bounds_.low[0]) / ompl::magic::PROJECTION_DIMENSION_SPLITS;
            cellSizes_[1] = (bounds_.high[1] - bounds_.low[1]) / ompl::magic::PROJECTION_DIMENSION_SPLITS;
            cellSizes_[2] = 1;
            //cellSizes_[2] = (bounds_.high[2] - bounds_.low[2]) / 2;//ompl::magic::PROJECTION_DIMENSION_SPLITS;
        }

        virtual void project(const ob::State *state, ob::EuclideanProjection &projection) const
        {
            //memcpy(&projection(0), state->as<DubinsZStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(0)->values, 2 * sizeof(double));
            projection[0] = state->as<DubinsZStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(0)->values[0];
            projection[1] = state->as<DubinsZStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(0)->values[1];
            projection[2] = 0;
        }
    };

    registerDefaultProjection(ob::ProjectionEvaluatorPtr(dynamic_cast<ob::ProjectionEvaluator*>(new DubinsZDefaultProjection(this))));
}


void DubinsZMotionValidator::DefaultSettings(void) {
  stateSpace_ = dynamic_cast<DubinsZStateSpace*>(si_->getStateSpace().get());
  if (!stateSpace_)
    throw ompl::Exception("No state space for motion validator");
}

bool DubinsZMotionValidator::checkMotion(const ob::State *s1, const ob::State *s2, std::pair<ob::State*, double> &lastValid) const {
  /* assume motion starts in a valid configuration so s1 is valid */
  bool result = true, firstTime = true, solved = true;
  ob::DubinsStateSpace::DubinsPath path;
  int nd = stateSpace_->validSegmentCount(s1, s2);

  if (stateSpace_->distance(s1, s2) > 0.5*std::numeric_limits<double>::max()){
    stateSpace_->copyState(lastValid.first, s1);
    lastValid.second = 0;
    return false;
  }

  if (nd > 1) {
    /* temporary storage for the checked state */
    ob::State *test = si_->allocState();

    for (int j = 1 ; j < nd ; ++j)
    {
      stateSpace_->interpolate(s1, s2, (double)j / (double)nd, firstTime, path, test);
      if (!solved) {
        result = false;
        break;
      }
      if (!si_->isValid(test))
      {
        lastValid.second = (double)(j - 1) / (double)nd;
        if (lastValid.first)
          stateSpace_->interpolate(s1, s2, lastValid.second, firstTime, path, lastValid.first);
        result = false;
        break;
      }
    }
    si_->freeState(test);
  }

  if (result && solved)
    if (!si_->isValid(s2))
    {
      lastValid.second = (double)(nd - 1) / (double)nd;
      if (lastValid.first)
        stateSpace_->interpolate(s1, s2, lastValid.second, firstTime, path, lastValid.first);
      result = false;
    }

  if (result)
    valid_++;
  else
    invalid_++;

  if(!solved) {
    stateSpace_->copyState(lastValid.first, s1);
    lastValid.second = 0;
  }

  return result;
}

bool DubinsZMotionValidator::checkMotion(const ob::State *s1, const ob::State *s2) const {
  /* assume motion starts in a valid configuration so s1 is valid */
  if (!si_->isValid(s2))
    return false;

  bool result = true, firstTime = true, solved = true;
  ob::DubinsStateSpace::DubinsPath path;
  int nd = stateSpace_->validSegmentCount(s1, s2);

  if (stateSpace_->distance(s1, s2) > 0.5*std::numeric_limits<double>::max())
    return false;


  /* initialize the queue of test positions */
  std::queue< std::pair<int, int> > pos;
  if (nd >= 2)
  {
    pos.push(std::make_pair(1, nd - 1));

    /* temporary storage for the checked state */
    ob::State *test = si_->allocState();

    /* repeatedly subdivide the path segment in the middle (and check the middle) */
    while (!pos.empty())
    {
      std::pair<int, int> x = pos.front();

      int mid = (x.first + x.second) / 2;
      stateSpace_->interpolate(s1, s2, (double)mid / (double)nd, firstTime, path, test);

      if (!solved || !si_->isValid(test))
      {
        result = false;
        break;
      }

      pos.pop();

      if (x.first < mid)
        pos.push(std::make_pair(x.first, mid - 1));
      if (x.second > mid)
        pos.push(std::make_pair(mid + 1, x.second));
    }

    si_->freeState(test);
  }

  if (result)
    valid_++;
  else
    invalid_++;

  return result;
}


unsigned int DubinsZMotionValidator::getMotionStates(const ompl::base::State *s1, const ompl::base::State *s2, std::vector<ompl::base::State*> &states, unsigned int count, bool endpoints, bool alloc) const {
  // add 1 to the number of states we want to add between s1 & s2. This gives us the number of segments to split the motion into
  count++;
  ob::DubinsStateSpace::DubinsPath path;
  bool firstTime = true, solved = true;
  if (count < 2)
  {
    unsigned int added = 0;

    // if they want endpoints, then at most endpoints are included
    if (endpoints)
    {
      if (alloc)
      {
        states.resize(2);
        states[0] = si_->allocState();
        states[1] = si_->allocState();
      }
      if (states.size() > 0)
      {
        si_->copyState(states[0], s1);
        added++;
      }

      if (states.size() > 1)
      {
        si_->copyState(states[1], s2);
        added++;
      }
    }
    else
      if (alloc)
        states.resize(0);
    return added;
  }

  if (alloc)
  {
    states.resize(count + (endpoints ? 1 : -1));
    if (endpoints)
      states[0] = si_->allocState();
  }

  unsigned int added = 0;

  if (endpoints && states.size() > 0)
  {
    si_->copyState(states[0], s1);
    added++;
  }

  /* find the states in between */
  for (unsigned int j = 1 ; j < count && added < states.size() ; ++j)
  {
    if (alloc)
      states[added] = si_->allocState();
    stateSpace_->interpolate(s1, s2, (double)j / (double)count, firstTime, path, states[added]);

    if (solved) {
      added++;
    } else {
      for (unsigned int l = 0; l <= added; l++)
        si_->freeState(states[l]);
      states.clear();
      added = 0;
      return added;
    }
  }

  if (added < states.size() && endpoints)
  {
    if (alloc)
      states[added] = si_->allocState();
    si_->copyState(states[added], s2);
    added++;
  }

  return added;

}


/*************************
 * Task space dubins z motion validator
 */


bool TaskSpaceDubinsZMotionValidator::CheckMotion(const ompl::base::State *s1, const ompl::base::State *s2, ompl::base::State *new_state) const {
  /* assume motion starts in a valid configuration so s1 is valid */
  if (!si_->isValid(s2))
    return false;

  bool result = true, firstTime = true;
  pc::TaskSpaceDubinsMotionValidator::SingleDubinsPath path;
  int nd = stateSpace_->validSegmentCount(s1, s2);

  /* initialize the queue of test positions */
  std::queue< std::pair<int, int> > pos;
  if (nd >= 2)
  {
    pos.push(std::make_pair(1, nd - 1));

    /* temporary storage for the checked state */
    ob::State *test = si_->allocState();

    /* repeatedly subdivide the path segment in the middle (and check the middle) */
    while (!pos.empty())
    {
      std::pair<int, int> x = pos.front();

      int mid = (x.first + x.second) / 2;
      Interpolate(s1, s2, (double)mid / (double)nd, firstTime, path, test);

      if (!si_->isValid(test))
      {
        result = false;
        break;
      }
      pos.pop();

      if (x.first < mid)
        pos.push(std::make_pair(x.first, mid - 1));
      if (x.second > mid)
        pos.push(std::make_pair(mid + 1, x.second));
    }

    si_->freeState(test);
  }

  if (result)
    Interpolate(s1, s2, 1.0, firstTime, path, new_state);

  return result;
}

void TaskSpaceDubinsZMotionValidator::GetEndState(const ompl::base::State *state1, const ompl::base::State *state2, ompl::base::State *new_state) const {
  const DubinsZStateSpace::StateType *s1 = static_cast<const DubinsZStateSpace::StateType*>(state1);
  const DubinsZStateSpace::StateType *s2 = static_cast<const DubinsZStateSpace::StateType*>(state2);

  ob::State *dub_s1 = stateSpace_->dubins_space_->allocState();
  ob::State *dub_s2 = stateSpace_->dubins_space_->allocState();
  ob::State *dub_res = stateSpace_->dubins_space_->allocState();


  dub_s1->as<ob::DubinsStateSpace::StateType>()->setXY(s1->GetTranslation().values[0], s1->GetTranslation().values[1]);
  dub_s1->as<ob::DubinsStateSpace::StateType>()->setYaw(s1->GetRotation().value);

  dub_s2->as<ob::DubinsStateSpace::StateType>()->setXY(s2->GetTranslation().values[0], s2->GetTranslation().values[1]);
  dub_s2->as<ob::DubinsStateSpace::StateType>()->setYaw(s2->GetRotation().value);

  tsmv_->GetEndState(dub_s1, dub_s2, dub_res);

  new_state->as<DubinsZStateSpace::StateType>()->GetTranslation().values[0] = dub_res->as<ob::DubinsStateSpace::StateType>()->getX();
  new_state->as<DubinsZStateSpace::StateType>()->GetTranslation().values[1] = dub_res->as<ob::DubinsStateSpace::StateType>()->getY();
  new_state->as<DubinsZStateSpace::StateType>()->GetRotation().value = dub_res->as<ob::DubinsStateSpace::StateType>()->getYaw();
  new_state->as<DubinsZStateSpace::StateType>()->GetTranslation().values[2] = s2->GetTranslation().values[2];
  stateSpace_->dubins_space_->freeState(dub_s1);
  stateSpace_->dubins_space_->freeState(dub_s2);
  stateSpace_->dubins_space_->freeState(dub_res);
}


double TaskSpaceDubinsZMotionValidator::Distance(const ob::State *state1, const ob::State *state2) const {
  const DubinsZStateSpace::StateType *s1 = static_cast<const DubinsZStateSpace::StateType*>(state1);
  const DubinsZStateSpace::StateType *s2 = static_cast<const DubinsZStateSpace::StateType*>(state2);

  ob::State *dub_s1 = stateSpace_->dubins_space_->allocState();
  ob::State *dub_s2 = stateSpace_->dubins_space_->allocState();

  dub_s1->as<ob::DubinsStateSpace::StateType>()->setXY(s1->GetTranslation().values[0], s1->GetTranslation().values[1]);
  dub_s1->as<ob::DubinsStateSpace::StateType>()->setYaw(s1->GetRotation().value);

  dub_s2->as<ob::DubinsStateSpace::StateType>()->setXY(s2->GetTranslation().values[0], s2->GetTranslation().values[1]);
  dub_s2->as<ob::DubinsStateSpace::StateType>()->setYaw(s2->GetRotation().value);

  double xydist = tsmv_->Distance(dub_s1, dub_s2);
  double zdist = s2->GetTranslation().values[2] - s1->GetTranslation().values[2];

  stateSpace_->dubins_space_->freeState(dub_s1);
  stateSpace_->dubins_space_->freeState(dub_s2);
  if ((zdist > 0 && zdist > stateSpace_->max_slope_pos_z_*xydist) ||
      (zdist < 0 && zdist < -stateSpace_->max_slope_neg_z_*xydist))
    return 0.6*std::numeric_limits<double>::max();
  else
    return sqrt(xydist*xydist + zdist*zdist);
}

void TaskSpaceDubinsZMotionValidator::Interpolate(const ompl::base::State *from, const ompl::base::State *to, const double t, bool& firstTime,
                                                  pc::TaskSpaceDubinsMotionValidator::SingleDubinsPath& path, ompl::base::State *state) const {
  if (firstTime) {
    if (t<=0.) {
      if (from != state)
        stateSpace_->copyState(state, from);
      return;
    }

    const DubinsZStateSpace::StateType *s1 = static_cast<const DubinsZStateSpace::StateType*>(from);
    const DubinsZStateSpace::StateType *s2 = static_cast<const DubinsZStateSpace::StateType*>(to);

    ob::State *dub_s1 = stateSpace_->dubins_space_->allocState();
    ob::State *dub_s2 = stateSpace_->dubins_space_->allocState();

    dub_s1->as<ob::DubinsStateSpace::StateType>()->setXY(s1->GetTranslation().values[0], s1->GetTranslation().values[1]);
    dub_s1->as<ob::DubinsStateSpace::StateType>()->setYaw(s1->GetRotation().value);

    dub_s2->as<ob::DubinsStateSpace::StateType>()->setXY(s2->GetTranslation().values[0], s2->GetTranslation().values[1]);
    dub_s2->as<ob::DubinsStateSpace::StateType>()->setYaw(s2->GetRotation().value);

    path = tsmv_->ComputeSingleDubinsPath(from, to);

    stateSpace_->dubins_space_->freeState(dub_s1);
    stateSpace_->dubins_space_->freeState(dub_s2);

    firstTime = false;
  }
  Interpolate(from, to, path, t, state);
}

void TaskSpaceDubinsZMotionValidator::Interpolate(const ompl::base::State *from, const ompl::base::State *to, const pc::TaskSpaceDubinsMotionValidator::SingleDubinsPath& path, double t, ompl::base::State *state) const {
  const DubinsZStateSpace::StateType *s = static_cast<const DubinsZStateSpace::StateType*>(from);
  const DubinsZStateSpace::StateType *s_to = static_cast<const DubinsZStateSpace::StateType*>(to);

  ob::State *dub_s = stateSpace_->dubins_space_->allocState();
  ob::State *dub_res = stateSpace_->dubins_space_->allocState();


  dub_s->as<ob::DubinsStateSpace::StateType>()->setXY(s->GetTranslation().values[0], s->GetTranslation().values[1]);
  dub_s->as<ob::DubinsStateSpace::StateType>()->setYaw(s->GetRotation().value);
  tsmv_->Interpolate(dub_s, path, t, dub_res);

  state->as<DubinsZStateSpace::StateType>()->GetTranslation().values[0] = dub_res->as<ob::DubinsStateSpace::StateType>()->getX();
  state->as<DubinsZStateSpace::StateType>()->GetTranslation().values[1] = dub_res->as<ob::DubinsStateSpace::StateType>()->getY();
  state->as<DubinsZStateSpace::StateType>()->GetRotation().value = dub_res->as<ob::DubinsStateSpace::StateType>()->getYaw();
  state->as<DubinsZStateSpace::StateType>()->GetTranslation().values[2] = ((1-t)*s->GetTranslation().values[2] + t*s_to->GetTranslation().values[2]);

  stateSpace_->dubins_space_->freeState(dub_s);
  stateSpace_->dubins_space_->freeState(dub_res);
}

void TaskSpaceDubinsZMotionValidator::DefaultSettings(void) {
  stateSpace_ = dynamic_cast<DubinsZStateSpace*>(si_->getStateSpace().get());
  if (!stateSpace_)
    throw ompl::Exception("No state space for motion validator");
  ob::SpaceInformationPtr si_dub(new ob::SpaceInformation(stateSpace_->dubins_space_));
  tsmv_.reset(new pc::TaskSpaceDubinsMotionValidator(si_dub));
}



}




