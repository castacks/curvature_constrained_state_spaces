/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * glideslope_bvp_solver.cpp
 *
 *  Created on: Mar 16, 2015
 *      Author: Sanjiban Choudhury
 */

#include "curvature_constrained_state_spaces/glideslope_bvp_solver.h"

#include <nlopt.hpp> // C++ interface for nlopt
#include <iostream>

#include "tictoc_profiler/profiler.hpp"


namespace ca {

bool GlideSlopeBVPSolver::SolveBVP(const BVP& start, const BVP& end, double distance, Trajectory &traj) const{
  // Step 1. Initial guess 0 0 0 0 (xf(1)-x0(1))/s
  std::vector<double> p0 {(end.z - start.z)/distance};

  // Step 4. Lowerbound lb = [0 0 -model.gamma_dot -model.gamma_dot -model.max_gamma];

  std::vector<double> lb {-glide_slope_max_};
  std::vector<double> ub {glide_slope_max_};

  // Step 5. Setup optimization
  nlopt::opt opt(nlopt::LN_COBYLA, p0.size());
  opt.set_lower_bounds(lb);
  opt.set_upper_bounds(ub);
  // set objective function

  OptData data;
  data.x0 = start;
  data.xf = end;
  data.dis = distance;
  data.glide_slope_rate_max = glide_slope_rate_max_;

  opt.set_min_objective(CostFunction, &data);
  //std::vector<double> tol_ineq(1, 1e-6);
  //opt.add_inequality_mconstraint(ConstraintFunction, &data, tol_ineq);
  //std::vector<double> tol_eq(1, 1e-3);
  //opt.add_equality_mconstraint(EqConstraintFunction, &data, tol_eq);
  opt.set_maxtime(options_.computation_time);
  opt.set_xtol_abs(options_.parameter_tolerance);
  opt.set_ftol_rel(options_.rel_function_tolerance);

  double minf;
  try {
    nlopt::result result = opt.optimize(p0, minf);
    if ((result == nlopt::SUCCESS || result == nlopt::FTOL_REACHED || result == nlopt::XTOL_REACHED)
        && opt.last_optimum_value() < 1){
      std::vector<double> p = p0;
      std::cout<< opt.last_optimum_value() <<"\n";
      std::cout<<p0[0]<<"\n";
      traj.s1 = fabs((p[0] - start.glide_slope)/glide_slope_rate_max_);
      traj.s2 = fabs((end.glide_slope - p[0])/glide_slope_rate_max_);
      int sign_a1 = (p[0] - start.glide_slope) > 0 ? 1 : -1;
      traj.a1 = sign_a1*glide_slope_rate_max_;
      int sign_a2 = (end.glide_slope - p[0]) > 0 ? 1 : -1;
      traj.a2 = sign_a2*glide_slope_rate_max_;
      traj.gamma = p0[0];
      traj.s = distance;
      traj.start = start;
      traj.end = end;
      return true;
    } else {
      return false;
    }
  }  catch (...) {
    //std::cout << "Exception occurred "<<e<<"\n";
    return false;
  }
}

double GlideSlopeBVPSolver::Length(const Trajectory &traj) {
  double res1 = traj.s1/100.0;
  double length = 0;
  for(double s1 = 0; s1 < traj.s1; s1+=res1)
    length += res1*sqrt(1 + (traj.start.glide_slope + traj.a1*s1)*(traj.start.glide_slope + traj.a1*s1));
  length += sqrt(1 + (traj.gamma)*(traj.gamma))*(traj.s - traj.s1 - traj.s2);
  double res2 = traj.s2/100.0;
  for(double s2 = 0; s2 < traj.s2; s2+=res2)
    length += res2*sqrt(1 + (traj.start.glide_slope + traj.a2*s2)*(traj.start.glide_slope + traj.a2*s2));
  return length;
}


GlideSlopeBVPSolver::BVP GlideSlopeBVPSolver::Interpolate(const Trajectory &traj, double s) const{
  BVP pt;
  double s_actual = s*traj.s;
  if (s <=0 ) {
    pt = traj.start;
  } else if (s < traj.s1/traj.s) {
    pt.glide_slope = traj.start.glide_slope + traj.a1*s_actual;
    pt.z = traj.start.z + traj.start.glide_slope*s_actual + 0.5*traj.a1*traj.a1*s_actual;
  } else if (s < (traj.s - traj.s2)/traj.s) {
    pt.glide_slope = traj.gamma;
    pt.z = traj.start.z + traj.start.glide_slope*traj.s1 + 0.5*traj.a1*traj.a1*traj.s1 + traj.gamma*(s_actual - traj.s1);
  } else if (s < 1) {
    pt.glide_slope = traj.gamma + traj.a2*(s_actual - (traj.s - traj.s2));
    pt.z = traj.start.z + traj.start.glide_slope*traj.s1 + 0.5*traj.a1*traj.a1*traj.s1 + traj.gamma*(traj.s - traj.s2) +
        traj.gamma*(s_actual - (traj.s - traj.s2)) + 0.5*traj.a2*traj.a2*(s_actual - (traj.s - traj.s2));
  } else if (s >= 1) {
    return traj.end;
  }
  return pt;
}

double GlideSlopeBVPSolver::CostFunction(const std::vector<double> &p, std::vector<double> &grad, void *f_data) {
  OptData *data = static_cast<OptData*> (f_data);
  double s1 = fabs((p[0] - data->x0.glide_slope)/data->glide_slope_rate_max);
  int sign_a1 = (p[0] - data->x0.glide_slope) > 0 ? 1 : -1;
  double delta_z1 = data->x0.glide_slope*s1 + sign_a1*0.5*s1*s1*data->glide_slope_rate_max;

  double s2 = fabs((data->xf.glide_slope - p[0])/data->glide_slope_rate_max);
  int sign_a2 = (data->xf.glide_slope - p[0]) > 0 ? 1 : -1;
  double delta_z3 = p[0]*s2 + sign_a2*0.5*s2*s2*data->glide_slope_rate_max;
  double delta_z2 = p[0]*(data->dis-s1-s2);
  double res = data->x0.z  + delta_z1 + delta_z2 + delta_z3 - data->xf.z;
  return res*res;
}



}


