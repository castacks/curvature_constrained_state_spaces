/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * stress_test_bvp.cpp
 *
 *  Created on: Nov 29, 2014
 *      Author: Sanjiban Choudhury
 */

#include "curvature_constrained_state_spaces/curvature_bvp_solver.h"
#include "math.h"
#include "tictoc_profiler/profiler.hpp"
#include "random_util/simple_random.hpp"
#include <boost/math/constants/constants.hpp>

using namespace ca;
int main(int argc, char **argv) {

  CurvatureBVPSolver bvp_solver(0.002265522456300,  2.739468793930299e-05);

  CurvatureBVPSolver::Options options;
  options.dist_threshold = 5.0;
  options.dot_threshold = 1e-2;
  options.computation_time = 0.01;
  options.parameter_tolerance = 1e-3;
  options.rel_function_tolerance = 1e-3;
  bvp_solver.SetOptions(options);

  ca::Profiler::enable();
  unsigned int num_trials = 10000;
  ca::sr::ClockSeed();
  int failures = 0;
  for (unsigned int trial = 0; trial < num_trials; trial++) {
    CurvatureBVPSolver::BVP start, end;

    start.x = ca::sr::Uniform(-2000, 2000);
    start.y = ca::sr::Uniform(-2000, 2000);
    start.psi = ca::sr::Uniform(-boost::math::constants::pi<float>(), boost::math::constants::pi<float>());
    start.curvature = ca::sr::Uniform(-0.002265522456300, 0.002265522456300);

    end.x = ca::sr::Uniform(-2000, 2000);
    end.y = ca::sr::Uniform(-2000, 2000);
    end.psi = ca::sr::Uniform(-boost::math::constants::pi<float>(), boost::math::constants::pi<float>());
    end.curvature = ca::sr::Uniform(-0.002265522456300, 0.002265522456300);

    CurvatureBVPSolver::Trajectory traj;
    ca::Profiler::tictoc("opt");
    bool result = bvp_solver.SolveBVP(start, end, traj);
    ca::Profiler::tictoc("opt");

    if (result) {
      double s_total = CurvatureBVPSolver::Length(traj);
      std::vector<Eigen::Vector3d> points;
      ca::Profiler::tictoc("sample");
      for (double alpha = 0; alpha <= 1; alpha += 0.001) {
        points.push_back(bvp_solver.Interpolate(traj, alpha*s_total));
      }
      ca::Profiler::tictoc("sample");
    } else {
      failures ++;
    }
  }

  ca::Profiler::print_aggregated(std::cout);
  std::cout<<"failures "<<failures*100.0/num_trials<<"% \n";
}



