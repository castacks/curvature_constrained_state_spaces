/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * example_clothoid_path.cpp
 *
 *  Created on: Nov 30, 2014
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>

#include "planning_common/paths/path_waypoint.h"
#include "planning_common/utils/visualization_utils.h"
#include "ompl/base/State.h"
#include "ompl/base/ScopedState.h"
#include "curvature_constrained_state_spaces/clothoid_state_space.h"
#include <boost/math/constants/constants.hpp>

using namespace ca;

namespace pc = planning_common;
namespace vu = pc::visualization_utils;
namespace ob = ompl::base;

int main(int argc, char **argv) {
  ros::init(argc, argv, "example_clothoid_path");
  ros::NodeHandle n("~");

  ros::Publisher pub_path = n.advertise<visualization_msgs::Marker>("path", 0);
  ros::Duration(1.0).sleep();

  // Defining clothoid space for speed 50m/s and roll limit 30 deg, roll rate limit 15 deg/s
  ob::StateSpacePtr space(new ClothoidStateSpace(0.002265522456300,  2.739468793930299e-05));
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  ob::MotionValidatorPtr mvp(new ClothoidMotionValidator(si.get()));
  si->setMotionValidator(mvp);

  ob::RealVectorBounds pos_bounds(2);
  pos_bounds.setLow(-10000);
  pos_bounds.setHigh(10000);

  space->as<ClothoidStateSpace>()->SetBounds(pos_bounds);

  si->setup();

  // Create start state and goal state
  ob::ScopedState<ClothoidStateSpace> start(space), goal(space);

  start->GetPose().setXY(100, 200);
  start->GetPose().setYaw(0.25*boost::math::constants::pi<double>());
  start->SetCurvature(0);

  goal->GetPose().setXY(-400, -200);
  goal->GetPose().setYaw(-0.25*boost::math::constants::pi<double>());
  goal->SetCurvature(0);

  // Create the path
  ompl::base::ScopedState<> s1 = start;
  ompl::base::ScopedState<> s2 = goal;
  pc::PathWaypoint path(si, s1.get(), s2.get());

  // Lets create a dense set of waypoints
  path.Interpolate(1000);

  // Print path
  path.PrintAsMatrix(std::cout);

  // Visualize
  pub_path.publish(vu::GetMarker(path, 1));
  ros::Duration(1.0).sleep();
}




