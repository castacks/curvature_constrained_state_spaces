/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * example_dubins3d_path.cpp
 *
 *  Created on: Jul 16, 2014
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>

#include "planning_common/paths/path_waypoint.h"
#include "planning_common/utils/visualization_utils.h"
#include "ompl/base/State.h"
#include "ompl/base/ScopedState.h"
#include "curvature_constrained_state_spaces/dubins_z_state_space.h"


using namespace ca;

namespace pc = planning_common;
namespace vu = pc::visualization_utils;
namespace ob = ompl::base;

int main(int argc, char **argv) {
  ros::init(argc, argv, "example_dubins_z_path");
  ros::NodeHandle n("~");

  ros::Publisher pub_path = n.advertise<visualization_msgs::Marker>("path", 0);
  ros::Duration(1.0).sleep();

  ob::StateSpacePtr space(new DubinsZStateSpace(1));
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));

  ob::RealVectorBounds pos_bounds(3);
  pos_bounds.setLow(-100);
  pos_bounds.setHigh(100);

  space->as<DubinsZStateSpace>()->SetBounds(pos_bounds);

  si->setup();

  // Create start state and goal state
  ob::ScopedState<DubinsZStateSpace> start(space), goal(space);

  start->GetTranslation().values[0] = 0;
  start->GetTranslation().values[1] = 0;
  start->GetTranslation().values[2] = 0;
  start->GetRotation().value = 0;

  goal->GetTranslation().values[0] = 1;
  goal->GetTranslation().values[1] = 1;
  goal->GetTranslation().values[2] = 1;
  goal->GetRotation().value = M_PI;

  // Create the path
  ompl::base::ScopedState<> s1 = start;
  ompl::base::ScopedState<> s2 = goal;
  pc::PathWaypoint path(si, s1.get(), s2.get());

  // Lets create a dense set of waypoints
  path.Interpolate(100);

  // Print path
  path.PrintAsMatrix(std::cout);

  // Visualize
  pub_path.publish(vu::GetMarker(path, 0.01));
  ros::Duration(1.0).sleep();
}





