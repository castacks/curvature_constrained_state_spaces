/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * example_nlopt.cpp
 *
 *  Created on: Nov 29, 2014
 *      Author: Sanjiban Choudhury
 */

#include <nlopt.hpp> // C++ interface for nlopt
#include <iostream>

static double ObjFunction(const std::vector<double> &x, std::vector<double> &grad, void *f_data) {
  return (x[0]-5)*(x[0]-5);
}

int main(int argc, char **argv) {
  nlopt::opt opt(nlopt::LN_COBYLA, 1);
  std::vector<double> lb { -5 };
  std::vector<double> ub { 5 };

  opt.set_lower_bounds(lb);
  opt.set_upper_bounds(ub);
  // set objective function
  double dummy;
  opt.set_min_objective(ObjFunction, &dummy);
  opt.set_maxtime(1.0);
  opt.set_xtol_abs(1e-3);

  std::vector<double> x {0.0};
  double minf;
  nlopt::result result = opt.optimize(x, minf);
  // check result value, if optimization was succesful
  std::cout<<"Resulting fval of Optimization: " << minf << std::endl;
  std::cout<<"Result of Optimization: " << result << std::endl;
  std::cout<<"x opt: " << x[0] << std::endl;
}



