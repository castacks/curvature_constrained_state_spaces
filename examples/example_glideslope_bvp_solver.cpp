/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * example_bvp_solver.cpp
 *
 *  Created on: Nov 29, 2014
 *      Author: Sanjiban Choudhury
 */

#include "curvature_constrained_state_spaces/glideslope_bvp_solver.h"
#include "math.h"
#include "tictoc_profiler/profiler.hpp"
#include <boost/math/constants/constants.hpp>

using namespace ca;
int main(int argc, char **argv) {

  GlideSlopeBVPSolver bvp_solver(5/30.0, 1/900.0);
  GlideSlopeBVPSolver::BVP start, end;
  start.z = 100;
  start.glide_slope = 0.1;

  end.z = 0;
  end.glide_slope = 0;

  double distance = 1200;

  GlideSlopeBVPSolver::Options options;
  options.computation_time = 0.1;
  options.parameter_tolerance = 1e-6;
  options.rel_function_tolerance = 1e-4;
  bvp_solver.SetOptions(options);

  GlideSlopeBVPSolver::Trajectory traj;

  ca::Profiler::enable();
  ca::Profiler::tictoc("opt");
  bool result = bvp_solver.SolveBVP(start, end, distance, traj);
  ca::Profiler::tictoc("opt");

  std::cout<<"Could solve? "<<result<<"\n";

  if (result) {
    std::cout<<"Sol "<<traj.s1<<" "<<traj.s2<<" "<<traj.a1<<" "<<traj.a2<<" "<<traj.gamma<<"\n";
  }

  ca::Profiler::print_aggregated(std::cout);
}



