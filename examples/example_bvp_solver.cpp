/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * example_bvp_solver.cpp
 *
 *  Created on: Nov 29, 2014
 *      Author: Sanjiban Choudhury
 */

#include "curvature_constrained_state_spaces/curvature_bvp_solver.h"
#include "math.h"
#include "tictoc_profiler/profiler.hpp"
#include <boost/math/constants/constants.hpp>

using namespace ca;
int main(int argc, char **argv) {

  CurvatureBVPSolver bvp_solver(0.002265522456300,  2.739468793930299e-05);
  CurvatureBVPSolver::BVP start, end;
  start.x = 100;
  start.y = 200;
  start.psi = 0.25*boost::math::constants::pi<double>();
  start.curvature = 0;//-0.002265522456300;

  end.x = -400;
  end.y = -200;
  end.psi = -0.25*boost::math::constants::pi<double>();
  end.curvature = 0;//0.002265522456300;

  CurvatureBVPSolver::Options options;
  options.dist_threshold = 5.0;
  options.dot_threshold = 1e-2;
  options.computation_time = 0.1;
  options.parameter_tolerance = 1e-3;
  options.rel_function_tolerance = 1e-3;
  bvp_solver.SetOptions(options);

  CurvatureBVPSolver::Trajectory traj;

  ca::Profiler::enable();
  ca::Profiler::tictoc("opt");
  bool result = bvp_solver.SolveBVP(start, end, traj);
  ca::Profiler::tictoc("opt");

  std::cout<<"Could solve? "<<result<<"\n";

  if (result) {
    for (auto it : traj) {
      std::cout<<"Type: "<<it.type<<" Offset: "<<it.offset.transpose()<<" distance: "<<it.distance<<" Curv: ";
      for (auto it2 : it.curv_set)
        std::cout<<it2<<" ";
      std::cout<<"\n";
    }

    double s_total = CurvatureBVPSolver::Length(traj);
    std::vector<Eigen::Vector3d> points;
    ca::Profiler::tictoc("sample");
    for (double alpha = 0; alpha <= 1; alpha += 0.001) {
      points.push_back(bvp_solver.Interpolate(traj, alpha*s_total));
    }
    ca::Profiler::tictoc("sample");

    for (auto it : points)
      std::cout<<it.transpose()<<"\n";
  }

  ca::Profiler::print_aggregated(std::cout);
}



