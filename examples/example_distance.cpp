/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * example_distance.cpp
 *
 *  Created on: Mar 15, 2015
 *      Author: Sanjiban Choudhury
 */

#include "planning_common/paths/path_waypoint.h"
#include "planning_common/utils/visualization_utils.h"
#include "ompl/base/State.h"
#include "ompl/base/ScopedState.h"
#include "curvature_constrained_state_spaces/clothoid_z_state_space.h"


using namespace ca;

namespace pc = planning_common;
namespace vu = pc::visualization_utils;
namespace ob = ompl::base;

int main(int argc, char **argv) {
  ompl::base::StateSpacePtr space(new ca::ClothoidZStateSpace(0.00632, 0.0764, 0.15));
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));

  ompl::base::RealVectorBounds bounds(3);
  double buffer = 20;
  bounds.setLow(0, -2000);
  bounds.setLow(1, -2000);
  bounds.setLow(2, 0);

  bounds.setHigh(0, 2000);
  bounds.setHigh(1, 2000);
  bounds.setHigh(2, 400);

  space->as<ca::ClothoidZStateSpace>()->SetBounds(bounds);
  si->setup();

  // set the start and goal states
  ompl::base::ScopedState<ca::ClothoidZStateSpace> startc(space), goalc(space);

  startc->GetTranslation().values[0] = -1307.7; startc->GetTranslation().values[1] = 1754.1; startc->GetTranslation().values[2] = 1;
  startc->GetRotation().value = -0.14983;
  startc->SetCurvature(0);

  goalc->GetTranslation().values[0] = -948.631; goalc->GetTranslation().values[1] = 1733.05; goalc->GetTranslation().values[2] = 108.094;
  goalc->GetRotation().value =  0.202218;
  goalc->SetCurvature(-0.0012933);

  std::cout<<si->trueDistance(startc.get(), goalc.get())<<"\n";
}


