/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * example_dubins_feasible_path_generator.cpp
 *
 *  Created on: Feb 1, 2015
 *      Author: Sanjiban Choudhury
 */

#include "curvature_constrained_state_spaces/dubins_feasible_path_generator.h"
#include "ompl/base/spaces/DubinsStateSpace.h"
#include "ompl/base/SpaceInformation.h"
#include "planning_common/paths/path_waypoint.h"
#include "planning_common/utils/visualization_utils.h"
#include <ros/ros.h>

using namespace ca;

namespace ob = ompl::base;
namespace pc = planning_common;
namespace vu = pc::visualization_utils;

bool InSquare(const ob::SpaceInformationPtr &si, const ob::State *state)
{
    const ob::DubinsStateSpace::StateType *s = state->as<ob::DubinsStateSpace::StateType>();
    double x=s->getX(), y=s->getY();
    //return si->satisfiesBounds(s);
    return si->satisfiesBounds(s) && (x >=0 && x<=500 && y>=0 && y<=500);
}


int main(int argc, char **argv) {
  ros::init(argc, argv, "example_dubins_feasible_path_generator");
  ros::NodeHandle n("~");

  ros::Publisher pub_path = n.advertise<visualization_msgs::Marker>("path", 0);
  ros::Duration(1.0).sleep();

  ob::StateSpacePtr space(new ob::DubinsStateSpace(50.0));
  ob::RealVectorBounds bounds(2);
  bounds.setLow(-10000);
  bounds.setHigh(100000);
  space->as<ob::DubinsStateSpace>()->setBounds(bounds);

  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setStateValidityChecker(boost::bind(&InSquare, si, _1));
  si->setup();

  ob::ScopedState<ob::DubinsStateSpace> start(space), goal(space);
  start->setXY(100, 50);
  start->setYaw(M_PI);

  goal->setXY(400, 25);
  goal->setYaw(0);

  DubinsFeasiblePathGenerator dfpg(si);
  pc::PathWaypoint path(si);

  bool result = dfpg.GetFeasiblePath(start.get(), goal.get(), path, 5.0);
  pub_path.publish(vu::GetMarker(path, 1, 0, 1, 0, 1));
  ros::Duration(1.0).sleep();
}



