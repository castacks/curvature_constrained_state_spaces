/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * glideslope_bvp_solver.h
 *
 *  Created on: Mar 16, 2015
 *      Author: Sanjiban Choudhury
 */

#ifndef CURVATURE_CONSTRAINED_STATE_SPACES_INCLUDE_CURVATURE_CONSTRAINED_STATE_SPACES_GLIDESLOPE_BVP_SOLVER_H_
#define CURVATURE_CONSTRAINED_STATE_SPACES_INCLUDE_CURVATURE_CONSTRAINED_STATE_SPACES_GLIDESLOPE_BVP_SOLVER_H_

#include <vector>
#include <utility>
#include <Eigen/Dense>

namespace ca {

class GlideSlopeBVPSolver {
 public:
  struct BVP {
    double z;
    double glide_slope;
  };

  struct Trajectory {
    BVP start;
    BVP end;
    double s1;
    double s2;
    double s;
    double a1;
    double a2;
    double gamma;
  };

  struct Options {
    double computation_time;
    double parameter_tolerance;
    double rel_function_tolerance;

    Options():
      computation_time(0.1),
      parameter_tolerance (1e-6),
      rel_function_tolerance (1e-4){}

  };

  GlideSlopeBVPSolver(double glide_slope_max, double glide_slope_rate_max)
  :glide_slope_max_(glide_slope_max),
   glide_slope_rate_max_(glide_slope_rate_max),
   options_(){}

  ~GlideSlopeBVPSolver() {}

  bool SolveBVP(const BVP& start, const BVP& end, double distance, Trajectory &trajectory) const;

  static double Length(const Trajectory &traj);
  BVP Interpolate(const Trajectory &traj, double s) const;


  void SetLimits(double glide_slope_max, double glide_slope_rate_max) {
    glide_slope_max_ = glide_slope_max;
    glide_slope_rate_max_ = glide_slope_rate_max;
  }

  void SetOptions(const Options &options) {
    options_ = options;
  }
 private:

  struct OptData {
    BVP x0;
    BVP xf;
    double dis;
    double glide_slope_rate_max;
  };

  static double CostFunction(const std::vector<double> &p, std::vector<double> &grad, void *f_data);

  static void ConstraintFunction(unsigned m, double *result, unsigned n, const double* p, double* grad, void* f_data);

  static void EqConstraintFunction(unsigned m, double *result, unsigned n, const double* p, double* grad, void* f_data);

  double glide_slope_max_;
  double glide_slope_rate_max_;
  Options options_;
};

}




#endif /* CURVATURE_CONSTRAINED_STATE_SPACES_INCLUDE_CURVATURE_CONSTRAINED_STATE_SPACES_GLIDESLOPE_BVP_SOLVER_H_ */
