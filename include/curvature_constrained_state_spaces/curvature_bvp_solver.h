/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * curvature_bvp_solver.h
 *
 *  Created on: Nov 29, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef CURVATURE_CONSTRAINED_STATE_SPACES_INCLUDE_CURVATURE_BVP_SOLVER_H_
#define CURVATURE_CONSTRAINED_STATE_SPACES_INCLUDE_CURVATURE_BVP_SOLVER_H_

#include <vector>
#include <utility>
#include <Eigen/Dense>

namespace ca {

class CurvatureBVPSolver {
 public:
  struct Segment {
    enum {CLOTHOID = 0, ARC, STRAIGHT};
    Eigen::Vector3d offset;
    unsigned int type;
    std::vector<double> curv_set;
    double distance;
  };
  typedef std::vector<Segment> Trajectory;


  struct BVP {
    double x;
    double y;
    double psi;
    double curvature;
  };

  struct Options {
    double dist_threshold;
    double dot_threshold;
    double computation_time;
    double parameter_tolerance;
    double rel_function_tolerance;

    Options():
      dist_threshold(5),
      dot_threshold(1e-2),
      computation_time(0.1),
      parameter_tolerance (1e-3),
      rel_function_tolerance (1e-3){}

  };

  CurvatureBVPSolver(double curv_max = 0, double curv_rate_max = 0)
  :curv_max_(curv_max),
   curv_rate_max_(curv_rate_max),
   options_(){}

  ~CurvatureBVPSolver() {}

  bool SolveBVP(const BVP& start, const BVP& end, Trajectory &trajectory) const;

  static double Length(const Trajectory &traj);

  Eigen::Vector3d Interpolate(const Trajectory &traj, double s) const;

  std::pair<Eigen::Vector3d, double> InterpolateCurv(const Trajectory &traj, double s) const;


  void SetCurvLimits(double curv_max, double curv_rate_max) {
    curv_max_ = curv_max;
    curv_rate_max_ = curv_rate_max;
  }

  void SetOptions(const Options &options) {
    options_ = options;
  }
 private:

  struct OptData {
    Eigen::Vector3d x0;
    Eigen::Vector3d xf;
    std::vector<double> curv_param;
    double min_rad;
    double dist_threshold;
    double dot_threshold;
  };

  static Eigen::Vector3d IntegrateClothoid(const Eigen::Vector3d &x0, double K1, double K2, double sf, double seval, double delta_s);

  static Eigen::Vector3d IntegrateArc(const Eigen::Vector3d &x0, double K, double sf, double seval);

  static Eigen::Vector3d IntegrateStraight(const Eigen::Vector3d &x0, double sf, double seval);

  static double CostFunction(const std::vector<double> &p, std::vector<double> &grad, void *f_data);

  static void ConstraintFunction(unsigned m, double *result, unsigned n, const double* p, double* grad, void* f_data);


  double curv_max_;
  double curv_rate_max_;
  Options options_;
};

}


#endif  // CURVATURE_CONSTRAINED_STATE_SPACES_INCLUDE_CURVATURE_BVP_SOLVER_H_ 
