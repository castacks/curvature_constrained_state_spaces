/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * clothoid_state_space.h
 *
 *  Created on: Nov 29, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef CURVATURE_CONSTRAINED_STATE_SPACES_INCLUDE_CURVATURE_CONSTRAINED_STATE_SPACES_CLOTHOID_STATE_SPACE_H_
#define CURVATURE_CONSTRAINED_STATE_SPACES_INCLUDE_CURVATURE_CONSTRAINED_STATE_SPACES_CLOTHOID_STATE_SPACE_H_

#include <boost/math/constants/constants.hpp>

#include "ompl/base/spaces/SE2StateSpace.h"
#include "ompl/base/MotionValidator.h"
#include "ompl/base/StateSpace.h"
#include "ompl/base/spaces/RealVectorStateSpace.h"
#include "ompl/base/spaces/SE2StateSpace.h"
#include "planning_common/states/complex_state_space_types.h"
#include "curvature_constrained_state_spaces/curvature_bvp_solver.h"

namespace ca {

class ClothoidStateSpace : public ompl::base::CompoundStateSpace {
 public:
  class StateType : public ompl::base::CompoundStateSpace::StateType {
   public:
    StateType(void) : ompl::base::CompoundStateSpace::StateType() {}

    const ompl::base::SE2StateSpace::StateType& GetPose() const {
      return *as<ompl::base::SE2StateSpace::StateType>(0);
    }

    double GetCurvature() const {
      return as<ompl::base::RealVectorStateSpace::StateType>(1)->values[0];
    }

    ompl::base::SE2StateSpace::StateType& GetPose() {
      return *as<ompl::base::SE2StateSpace::StateType>(0);
    }

    void SetCurvature(double x) {
      as<ompl::base::RealVectorStateSpace::StateType>(1)->values[0] = x;
    }
  };


  ClothoidStateSpace(double curvature_max = 1.0, double curvature_rate_max = 1.0)
  : CompoundStateSpace(),
    curvature_max_(curvature_max),
    curvature_rate_max_(curvature_rate_max),
    bvp_solver_(curvature_max, curvature_rate_max) {
    setName("ClothoidStateSpace" + getName());
    type_ = planning_common::CLOTHOID_STATE_SPACE;

    ompl::base::StateSpacePtr space;

    space = ompl::base::StateSpacePtr(new ompl::base::SE2StateSpace());
    space->as<ompl::base::SE2StateSpace>()->AssignWorkspaceTagSubspace(ompl_base::WorkspaceTags::TRANSLATION, ompl_base::WorkspaceTags::ROTATION);
    addSubspace(space, 1.0);

    // Curvature
    space = ompl::base::StateSpacePtr(new ompl::base::RealVectorStateSpace(1));
    addSubspace(space, 1.0);

    lock();

    // Setup default bvp
    CurvatureBVPSolver::Options options;
    options.computation_time = 0.01;
    options.dist_threshold = (1.0/curvature_max_)/100.0;
    options.dot_threshold = 1e-2;
    options.parameter_tolerance = 1e-3;
    options.rel_function_tolerance = 1e-3;
    bvp_solver_.SetOptions(options);

    // Set curvature bounds since we know them
    ompl::base::RealVectorBounds curv_bounds(1);
    curv_bounds.setLow(-curvature_max);
    curv_bounds.setHigh(curvature_max);
    as<ompl::base::RealVectorStateSpace>(1)->setBounds(curv_bounds);

  }

  void SetBounds(const ompl::base::RealVectorBounds &position_bounds) {
    as<ompl::base::SE2StateSpace>(0)->setBounds(position_bounds);
  }

  virtual ~ClothoidStateSpace(void) {}


  virtual bool isMetricSpace(void) const { return false;}

  virtual double distance(const ompl::base::State *state1, const ompl::base::State *state2) const;

  virtual double trueDistance(const ompl::base::State *state1, const ompl::base::State *state2) const;

  virtual double heuristicDistance(const ompl::base::State *state1, const ompl::base::State *state2) const;


  virtual void interpolate(const ompl::base::State *from, const ompl::base::State *to, const double t,
                           ompl::base::State *state) const;
  virtual bool interpolate(const ompl::base::State *from, const ompl::base::State *to, const double t,
                           bool& firstTime, CurvatureBVPSolver::Trajectory& path, ompl::base::State *state) const;

  virtual bool hasSymmetricDistance(void) const { return false; }

  virtual bool hasSymmetricInterpolate(void) const { return false;}

  virtual void sanityChecks(void) const {}


  double curvature_max() {return curvature_max_;}
  double curvature_rate_max() {return curvature_rate_max_;}

  protected:
  virtual bool interpolate(const CurvatureBVPSolver::Trajectory& path, const double t,
                           ompl::base::State *state) const;

  double curvature_max_, curvature_rate_max_;
  CurvatureBVPSolver bvp_solver_;
};

/** \brief
    This motion validator is almost identical to the DiscreteMotionValidator
    except that it remembers the computed bvp path between different calls to
    interpolate. */
class ClothoidMotionValidator : public ompl::base::MotionValidator {
 public:
  ClothoidMotionValidator(ompl::base::SpaceInformation* si) : ompl::base::MotionValidator(si) {
    DefaultSettings();
 }
  ClothoidMotionValidator(const ompl::base::SpaceInformationPtr &si) : ompl::base::MotionValidator(si) {
    DefaultSettings();
  }
  virtual ~ClothoidMotionValidator(void) {}

  /** Copied from dubins implementation in ompl**/
  virtual bool checkMotion(const ompl::base::State *s1, const ompl::base::State *s2) const;

  /** Copied from dubins implementation in ompl**/
  virtual bool checkMotion(const ompl::base::State *s1, const ompl::base::State *s2, std::pair<ompl::base::State*, double> &lastValid) const;

  virtual unsigned int getMotionStates(const ompl::base::State *s1, const ompl::base::State *s2, std::vector<ompl::base::State*> &states, unsigned int count, bool endpoints, bool alloc) const;

 private:
  ClothoidStateSpace *stateSpace_;
  void DefaultSettings(void);
};


}


#endif  // CURVATURE_CONSTRAINED_STATE_SPACES_INCLUDE_CURVATURE_CONSTRAINED_STATE_SPACES_CLOTHOID_STATE_SPACE_H_ 
