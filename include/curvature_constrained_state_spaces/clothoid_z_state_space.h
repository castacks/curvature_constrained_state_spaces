/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * clothoid_z_state_space.h
 *
 *  Created on: Mar 15, 2015
 *      Author: Sanjiban Choudhury
 */

#ifndef CURVATURE_CONSTRAINED_STATE_SPACES_INCLUDE_CURVATURE_CONSTRAINED_STATE_SPACES_CLOTHOID_Z_STATE_SPACE_H_
#define CURVATURE_CONSTRAINED_STATE_SPACES_INCLUDE_CURVATURE_CONSTRAINED_STATE_SPACES_CLOTHOID_Z_STATE_SPACE_H_


#include <boost/math/constants/constants.hpp>

#include "ompl/base/MotionValidator.h"
#include "ompl/base/StateSpace.h"
#include "ompl/base/spaces/RealVectorStateSpace.h"
#include "planning_common/states/complex_state_space_types.h"
#include "curvature_constrained_state_spaces/clothoid_state_space.h"

namespace ca {

class ClothoidZStateSpace : public ompl::base::CompoundStateSpace {
 public:
  class StateType : public ompl::base::CompoundStateSpace::StateType {
   public:
    StateType(void) : ompl::base::CompoundStateSpace::StateType() {}

    const ompl::base::RealVectorStateSpace::StateType& GetTranslation() const {
      return *as<ompl::base::RealVectorStateSpace::StateType>(0);
    }

    const ompl::base::SO2StateSpace::StateType& GetRotation() const {
      return *as<ompl::base::SO2StateSpace::StateType>(1);
    }

    double GetCurvature() const {
      return as<ompl::base::RealVectorStateSpace::StateType>(2)->values[0];
    }

    ompl::base::RealVectorStateSpace::StateType& GetTranslation() {
      return *as<ompl::base::RealVectorStateSpace::StateType>(0);
    }

    ompl::base::SO2StateSpace::StateType& GetRotation() {
      return *as<ompl::base::SO2StateSpace::StateType>(1);
    }

    void SetCurvature(double x) {
      as<ompl::base::RealVectorStateSpace::StateType>(2)->values[0] = x;
    }
  };


  ClothoidZStateSpace(double curvature_max = 1.0, double curvature_rate_max = 1.0, double max_slope = std::numeric_limits<double>::max())
  : max_slope_(max_slope),
    CompoundStateSpace(){
    setName("ClothoidZStateSpace" + getName());
    type_ = planning_common::CLOTHOID_Z_STATE_SPACE;

    ompl::base::StateSpacePtr space;

    space = ompl::base::StateSpacePtr(new ompl::base::RealVectorStateSpace(3));
    space->as<ompl::base::RealVectorStateSpace>()->AssignWorkspaceTag(ompl_base::WorkspaceTags::TRANSLATION);
    addSubspace(space, 1.0);

    space = ompl::base::StateSpacePtr(new ompl::base::SO2StateSpace());
    space->as<ompl::base::SO2StateSpace>()->AssignWorkspaceTag(ompl_base::WorkspaceTags::ROTATION);
    addSubspace(space, 1.0);

    space = ompl::base::StateSpacePtr(new ompl::base::RealVectorStateSpace(1));
    addSubspace(space, 1.0);    // Set curvature bounds since we know them
    ompl::base::RealVectorBounds curv_bounds(1);
    curv_bounds.setLow(-curvature_max);
    curv_bounds.setHigh(curvature_max);
    as<ompl::base::RealVectorStateSpace>(2)->setBounds(curv_bounds);

    clothoid_space_.reset(new ca::ClothoidStateSpace(curvature_max, curvature_rate_max));

    lock();
  }

  void SetBounds(const ompl::base::RealVectorBounds &bounds) {
    as<ompl::base::RealVectorStateSpace>(0)->setBounds(bounds);
  }

  virtual ~ClothoidZStateSpace(void) {}


  virtual bool isMetricSpace(void) const { return false;}

  virtual double distance(const ompl::base::State *state1, const ompl::base::State *state2) const;

  virtual double trueDistance(const ompl::base::State *state1, const ompl::base::State *state2) const;

  virtual double heuristicDistance(const ompl::base::State *state1, const ompl::base::State *state2) const;


  virtual void interpolate(const ompl::base::State *from, const ompl::base::State *to, const double t,
                           ompl::base::State *state) const;
  virtual void interpolate(const ompl::base::State *from, const ompl::base::State *to, const double t,
                           bool& firstTime, CurvatureBVPSolver::Trajectory& path, ompl::base::State *state) const;

  virtual bool hasSymmetricDistance(void) const { return false; }

  virtual bool hasSymmetricInterpolate(void) const { return false;}

  virtual void sanityChecks(void) const {}
  protected:
  boost::shared_ptr<ca::ClothoidStateSpace> clothoid_space_;
  double max_slope_;
};

/** \brief
    This motion validator is almost identical to the DiscreteMotionValidator
    except that it remembers the computed bvp path between different calls to
    interpolate. */
class ClothoidZMotionValidator : public ompl::base::MotionValidator {
 public:
  ClothoidZMotionValidator(ompl::base::SpaceInformation* si) : ompl::base::MotionValidator(si) {
    DefaultSettings();
 }
  ClothoidZMotionValidator(const ompl::base::SpaceInformationPtr &si) : ompl::base::MotionValidator(si) {
    DefaultSettings();
  }
  virtual ~ClothoidZMotionValidator(void) {}

  /** Copied from dubins implementation in ompl**/
  virtual bool checkMotion(const ompl::base::State *s1, const ompl::base::State *s2) const;

  /** Copied from dubins implementation in ompl**/
  virtual bool checkMotion(const ompl::base::State *s1, const ompl::base::State *s2, std::pair<ompl::base::State*, double> &lastValid) const;

  virtual unsigned int getMotionStates(const ompl::base::State *s1, const ompl::base::State *s2, std::vector<ompl::base::State*> &states, unsigned int count, bool endpoints, bool alloc) const;

 private:
  ClothoidZStateSpace *stateSpace_;
  void DefaultSettings(void);
};


}




#endif /* CURVATURE_CONSTRAINED_STATE_SPACES_INCLUDE_CURVATURE_CONSTRAINED_STATE_SPACES_CLOTHOID_Z_STATE_SPACE_H_ */
