/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * dubins_feasible_path_generator.h
 *
 *  Created on: Jan 31, 2015
 *      Author: Sanjiban Choudhury
 */

#ifndef CURVATURE_CONSTRAINED_STATE_SPACES_INCLUDE_CURVATURE_CONSTRAINED_STATE_SPACES_DUBINS_FEASIBLE_PATH_GENERATOR_H_
#define CURVATURE_CONSTRAINED_STATE_SPACES_INCLUDE_CURVATURE_CONSTRAINED_STATE_SPACES_DUBINS_FEASIBLE_PATH_GENERATOR_H_

#include "planning_common/paths/feasible_path_generator.h"

namespace ca {

class DubinsFeasiblePathGenerator : public FeasiblePathGenerator {
 public:
  DubinsFeasiblePathGenerator (const ompl::base::SpaceInformationPtr &si)
 : FeasiblePathGenerator(si) {}
  virtual ~DubinsFeasiblePathGenerator() {}

  virtual bool GetFeasiblePath(const ompl::base::State *state1, const ompl::base::State *state2, planning_common::PathWaypoint &feasible_path, double resolution);
};

}  // namespace ca


#endif  // CURVATURE_CONSTRAINED_STATE_SPACES_INCLUDE_CURVATURE_CONSTRAINED_STATE_SPACES_DUBINS_FEASIBLE_PATH_GENERATOR_H_ 
