/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * dubins_z_state_space.h
 *
 *  Created on: Mar 14, 2015
 *      Author: Sanjiban Choudhury
 */

#ifndef CURVATURE_CONSTRAINED_STATE_SPACES_INCLUDE_CURVATURE_CONSTRAINED_STATE_SPACES_DUBINS_Z_STATE_SPACE_H_
#define CURVATURE_CONSTRAINED_STATE_SPACES_INCLUDE_CURVATURE_CONSTRAINED_STATE_SPACES_DUBINS_Z_STATE_SPACE_H_


#include <boost/math/constants/constants.hpp>

#include "ompl/base/MotionValidator.h"
#include "ompl/base/StateSpace.h"
#include "ompl/base/spaces/RealVectorStateSpace.h"
#include "ompl/base/spaces/DubinsStateSpace.h"
#include "planning_common/states/complex_state_space_types.h"
#include "curvature_constrained_state_spaces/curvature_bvp_solver.h"
#include "planning_common/motion_validator/task_space_dubins_motion_validator.h"

namespace ca {

class DubinsZStateSpace : public ompl::base::CompoundStateSpace {
 public:
  class StateType : public ompl::base::CompoundStateSpace::StateType {
   public:
    StateType(void) : ompl::base::CompoundStateSpace::StateType() {}

    const ompl::base::RealVectorStateSpace::StateType& GetTranslation() const {
      return *as<ompl::base::RealVectorStateSpace::StateType>(0);
    }

    const ompl::base::SO2StateSpace::StateType& GetRotation() const {
      return *as<ompl::base::SO2StateSpace::StateType>(1);
    }

    ompl::base::RealVectorStateSpace::StateType& GetTranslation() {
      return *as<ompl::base::RealVectorStateSpace::StateType>(0);
    }

    ompl::base::SO2StateSpace::StateType& GetRotation() {
      return *as<ompl::base::SO2StateSpace::StateType>(1);
    }
  };


  DubinsZStateSpace(double turning_radius,
                    double max_slope_pos_z,
                    double max_slope_neg_z)
  : max_slope_pos_z_(max_slope_pos_z),
    max_slope_neg_z_(max_slope_neg_z),
    CompoundStateSpace(){
    setName("DubinsZStateSpace" + getName());
    type_ = planning_common::DUBINS_Z_STATE_SPACE;

    ompl::base::StateSpacePtr space;

    space = ompl::base::StateSpacePtr(new ompl::base::RealVectorStateSpace(3));
    space->as<ompl::base::RealVectorStateSpace>()->AssignWorkspaceTag(ompl_base::WorkspaceTags::TRANSLATION);
    addSubspace(space, 1.0);

    space = ompl::base::StateSpacePtr(new ompl::base::SO2StateSpace());
    space->as<ompl::base::SO2StateSpace>()->AssignWorkspaceTag(ompl_base::WorkspaceTags::ROTATION);
    addSubspace(space, 1.0);

    dubins_space_.reset(new ompl::base::DubinsStateSpace(turning_radius));

    lock();
  }

  DubinsZStateSpace(double turning_radius,
                    double max_slope_pos_z)
  :DubinsZStateSpace(turning_radius, max_slope_pos_z, max_slope_pos_z)
  {}

  DubinsZStateSpace(double turning_radius)
  :DubinsZStateSpace(turning_radius, std::numeric_limits<double>::max(), std::numeric_limits<double>::max())
  {}

  DubinsZStateSpace()
  :DubinsZStateSpace(1.0, std::numeric_limits<double>::max(), std::numeric_limits<double>::max())
  {}

  void SetBounds(const ompl::base::RealVectorBounds &bounds) {
    as<ompl::base::RealVectorStateSpace>(0)->setBounds(bounds);
  }

  const ompl::base::RealVectorBounds& getBounds(void) const
  {
      return as<ompl::base::RealVectorStateSpace>(0)->getBounds();
  }

  virtual ~DubinsZStateSpace(void) {}


  virtual bool isMetricSpace(void) const { return false;}

  virtual double distance(const ompl::base::State *state1, const ompl::base::State *state2) const;

  virtual double trueDistance(const ompl::base::State *state1, const ompl::base::State *state2) const;

  virtual double heuristicDistance(const ompl::base::State *state1, const ompl::base::State *state2) const;


  virtual void interpolate(const ompl::base::State *from, const ompl::base::State *to, const double t,
                           ompl::base::State *state) const;
  virtual void interpolate(const ompl::base::State *from, const ompl::base::State *to, const double t,
                           bool& firstTime, ompl::base::DubinsStateSpace::DubinsPath& path, ompl::base::State *state) const;

  virtual bool hasSymmetricDistance(void) const { return false; }

  virtual bool hasSymmetricInterpolate(void) const { return false;}

  virtual void sanityChecks(void) const {}

  virtual void registerProjections(void);

  protected:
  friend class TaskSpaceDubinsZMotionValidator;
  boost::shared_ptr<ompl::base::DubinsStateSpace> dubins_space_;
  double max_slope_pos_z_;
  double max_slope_neg_z_;
};

/** \brief
    This motion validator is almost identical to the DiscreteMotionValidator
    except that it remembers the computed bvp path between different calls to
    interpolate. */
class DubinsZMotionValidator : public ompl::base::MotionValidator {
 public:
  DubinsZMotionValidator(ompl::base::SpaceInformation* si) : ompl::base::MotionValidator(si) {
    DefaultSettings();
 }
  DubinsZMotionValidator(const ompl::base::SpaceInformationPtr &si) : ompl::base::MotionValidator(si) {
    DefaultSettings();
  }
  virtual ~DubinsZMotionValidator(void) {}

  /** Copied from dubins implementation in ompl**/
  virtual bool checkMotion(const ompl::base::State *s1, const ompl::base::State *s2) const;

  /** Copied from dubins implementation in ompl**/
  virtual bool checkMotion(const ompl::base::State *s1, const ompl::base::State *s2, std::pair<ompl::base::State*, double> &lastValid) const;

  virtual unsigned int getMotionStates(const ompl::base::State *s1, const ompl::base::State *s2, std::vector<ompl::base::State*> &states, unsigned int count, bool endpoints, bool alloc) const;

 private:
  DubinsZStateSpace *stateSpace_;
  void DefaultSettings(void);
};

class TaskSpaceDubinsZMotionValidator : public  planning_common::TaskSpaceMotionValidator {
 public:
  TaskSpaceDubinsZMotionValidator(ompl::base::SpaceInformation* si)
 : TaskSpaceMotionValidator(si) {
    DefaultSettings();
  }
  TaskSpaceDubinsZMotionValidator(const ompl::base:: SpaceInformationPtr &si)
  : TaskSpaceMotionValidator(si) {
    DefaultSettings();
  }
  virtual ~ TaskSpaceDubinsZMotionValidator(void)
  {
  }

  virtual bool CheckMotion(const ompl::base::State *s1, const ompl::base::State *s2, ompl::base::State *new_state) const;

  virtual void GetEndState(const ompl::base::State *s1, const ompl::base::State *s2, ompl::base::State *new_state) const;

  virtual double Distance(const ompl::base::State *state1, const ompl::base::State *state2) const;

  void Interpolate(const ompl::base::State *from, const ompl::base::State *to, const double t, bool& firstTime,
                   planning_common::TaskSpaceDubinsMotionValidator::SingleDubinsPath& path, ompl::base::State *state) const;

  void Interpolate(const ompl::base::State *from, const ompl::base::State *to, const planning_common::TaskSpaceDubinsMotionValidator::SingleDubinsPath& path, double t, ompl::base::State *state) const;

 private:
  DubinsZStateSpace *stateSpace_;
  planning_common::TaskSpaceDubinsMotionValidatorPtr tsmv_;
  void DefaultSettings(void);
};

typedef boost::shared_ptr<TaskSpaceDubinsZMotionValidator> TaskSpaceDubinsZMotionValidatorPtr;


}



#endif /* CURVATURE_CONSTRAINED_STATE_SPACES_INCLUDE_CURVATURE_CONSTRAINED_STATE_SPACES_DUBINS_Z_STATE_SPACE_H_ */
