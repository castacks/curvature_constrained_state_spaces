function xf = integrate_arc( x0, K, sf )
%INTEGRATE_ARC Summary of this function goes here
%   Detailed explanation goes here

xf = zeros(size(x0));
xf(1) = x0(1) + (1/K)*(sin(x0(3) + K*sf) - sin(x0(3)));
xf(2) = x0(2) - (1/K)*(cos(x0(3) + K*sf) - cos(x0(3)));
xf(3) = x0(3) + K*sf;
end

