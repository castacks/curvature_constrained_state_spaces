function path = get_path( x0, p, K, delta_s )
%GET_PATH Summary of this function goes here
%   Detailed explanation goes here

path = [];
path = [path path_clothoid(x0, K(1), K(2), p(1), delta_s)];
path = [path path_arc(path(:,end), K(2), p(2), delta_s)];
path = [path path_clothoid(path(:,end), K(2), K(3), p(3), delta_s)];
if (K(3) == 0)
    path = [path path_straight(path(:,end), p(4), delta_s)];
else
    path = [path path_arc(path(:,end), K(3), p(4), delta_s)];
end
path = [path path_clothoid(path(:,end), K(3), K(4), p(5), delta_s)];
path = [path path_arc(path(:,end), K(4), p(6), delta_s)];
path = [path path_clothoid(path(:,end), K(4), K(5), p(7), delta_s)];

end

function path = path_clothoid(x0, K1, K2, sf, delta_s)
%INTEGRATE_CLOTHOID Summary of this function goes here
%   Detailed explanation goes here

s = linspace(0, sf, 1+ceil((eps+sf)/delta_s));
ds = s(2)-s(1);
psi = x0(3) + K1*s + ((K2-K1)/(2*(eps+sf)))*s.^2;

path = [x0(1) + [0 cumsum(cos(psi(1:(end-1)))*ds)];
        x0(2) + [0 cumsum(sin(psi(1:(end-1)))*ds)];
        psi];
end

function path = path_arc( x0, K, sf, delta_s )
%INTEGRATE_ARC Summary of this function goes here
%   Detailed explanation goes here

s = linspace(0, sf, 1+ceil((eps+sf)/delta_s));
path = [x0(1) + (1/K)*(sin(x0(3) + K*s) - sin(x0(3)));
      x0(2) - (1/K)*(cos(x0(3) + K*s) - cos(x0(3)));
      x0(3) + K*s];
end

function path = path_straight( x0, sf, delta_s )
%INTEGRATE_STRAIGHT Summary of this function goes here
%   Detailed explanation goes here

s = linspace(0, sf, 1+ceil((eps+sf)/delta_s));

path = [x0(1) + s*cos(x0(3));
         x0(2) + s*sin(x0(3));
         x0(3)*ones(size(s))];
end

