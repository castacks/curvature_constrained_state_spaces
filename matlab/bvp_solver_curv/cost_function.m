function cost = cost_function( p , x0, xf, Kparam, model)
%COST_FUNCTION Summary of this function goes here
%   Detailed explanation goes here

distance = sum(p);

% Compute segment 1
% x1 = integrate_clothoid(x0, Kparam(1), Kparam(2), p(1), 0.1*model.r_min);
% x2 = integrate_arc(x1, Kparam(2), p(2));
% if (Kparam(3) == 0)
%     x3 = integrate_clothoid(x2, Kparam(2), 0, p(3), 0.1*model.r_min);
%     x4 = integrate_straight(x3, p(4));
%     x5 = integrate_clothoid(x4, 0, Kparam(4), p(5), 0.1*model.r_min);
% else
%     x3 = integrate_clothoid(x2, Kparam(2), Kparam(3), p(3), 0.1*model.r_min);
%     x4 = integrate_arc(x3, Kparam(3), p(4));
%     x5 = integrate_clothoid(x4, Kparam(3), Kparam(4), p(5), 0.1*model.r_min);
% end
% x6 = integrate_arc(x5, Kparam(4), p(6));
% x7 = integrate_clothoid(x6, Kparam(4), Kparam(5), p(7), 0.1*model.r_min);

% cost = distance + 1e4*(xf(1:2) - x7(1:2))'*(xf(1:2) - x7(1:2)) ...
%                 + 1e4*(1 - cos(xf(3))*cos(x7(3)) - sin(xf(3))*sin(x7(3)) );

cost = distance;
end

