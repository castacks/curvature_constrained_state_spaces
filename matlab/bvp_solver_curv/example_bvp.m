%% This solves a BVP that minimizes length while satisfying nonlinear constraints
clc;
clear;
close all;

%% Boundary conditions

% FIXME: The cyclic angle is causing a problem - have to change the end
% point constraints
model.V = 50;
model.g = 9.81;
model.psidot_max = (model.g / model.V) *tand(30);
model.psiddot_max = (model.g / model.V) * ((cosd(30))^(-2)) * deg2rad(15);
model.r_min = model.V / model.psidot_max;
model.Kdot = (model.psiddot_max / model.V) / model.V;


x0 = [0 0 0]'; curv0 = 0;
xf = [100 -100 pi]'; curvf = -1/model.r_min; 
%xf = [1000 0 pi]'; curvf = 0; 
%% Initialize trajectory (w/0 dubin)
% N = 100;
% d = 4;
% x_traj = [linspace(x0(1), xf(1), N+2); linspace(x0(2), xf(2), N+2); linspace(x0(3), x0(3), N+2); linspace(x0(4), x0(4), N+2)];
% x_traj(:,1) = []; x_traj(:,2) = []; 
% u_traj = linspace(0, 0, N+1);
% del_t = 1/N;
% p0 = pack_parameters( x_traj, u_traj, del_t );


%% Initialize trajectory (dubin)
[type, param] = dubins_analytic(x0(1:3)', xf(1:3)', model.r_min);

Kparam = [curv0];
if (type(1) == 0 )
    Kparam = [Kparam 1/model.r_min];
elseif (type(1) == 2 )
    Kparam = [Kparam -1/model.r_min];
end

if (type(2) == 0 )
    Kparam = [Kparam 1/model.r_min];
elseif (type(2) == 1 )
    Kparam = [Kparam 0];
elseif (type(2) == 2 )
    Kparam = [Kparam -1/model.r_min];
end

if (type(3) == 0 )
    Kparam = [Kparam 1/model.r_min];
elseif (type(3) == 2 )
    Kparam = [Kparam -1/model.r_min];
end

Kparam = [Kparam curvf];

p0 = [0 param(1)*model.r_min 0 param(2)*model.r_min 0 param(3)*model.r_min 0];

%% Cost and constraints
cost_fn = @(x) cost_function( x , x0, xf, Kparam, model);
threshold.dist_thresh = 1;
threshold.dot_thresh = 1e-3;
constraints_fn = @(x) bvp_constraint( x , x0, xf, Kparam, model, threshold);
A =   [-1 0  0 0  0 0  0;
        0 0 -1 0  0 0  0;
        0 0  0 0 -1 0  0;
        0 0  0 0  0 0 -1];
    
b = [-abs( Kparam(1) -  Kparam(2))/model.Kdot; -abs(Kparam(3) - Kparam(2))/model.Kdot; -abs(Kparam(3) - Kparam(4))/model.Kdot; -abs(Kparam(5) - Kparam(4))/model.Kdot];

%% Optimize
% set options for fmincon()
options = optimset('MaxFunEvals',1000000, 'TolFun', 1e-3,'MaxIter', 100, 'Display','iter');

% do optimization
ticid = tic;
[p,fval,exitflag]=fmincon(cost_fn, p0, A, b, [], [], zeros(size(p0)), [], constraints_fn, options);
toc(ticid)

%% Plot
path = get_path( x0, p, Kparam, 1 );
path_dubin = get_path( x0, p0, Kparam, 1 );

figure;
hold on;
plot(path(1,:), path(2,:),'g');
plot(path_dubin(1,:), path_dubin(2,:),'r');