function xf = integrate_straight( x0, sf)
%INTEGRATE_STRAIGHT Summary of this function goes here
%   Detailed explanation goes here

xf = zeros(size(x0));
xf(1) = x0(1) + sf*cos(x0(3));
xf(2) = x0(2) + sf*sin(x0(3));
xf(3) = x0(3);
end

