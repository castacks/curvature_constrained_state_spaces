function xf_act = get_end_state(p , x0, xf, guess)
%GET_END_STATE Summary of this function goes here
%   Detailed explanation goes here

x1 = integrate_clothoid(x0(1:3), x0(4), guess.K1, p(1));
x2 = integrate_arc(x1, guess.K1, p(2));
x3 = integrate_clothoid(x2, guess.K1, 0, p(3));
x4 = integrate_straight(x3, p(4));
x5 = integrate_clothoid(x4, 0, guess.K2, p(5));
x6 = integrate_arc(x5, guess.K2, p(6));
x7 = integrate_clothoid(x6, guess.K2, xf(4), p(7));

xf_act = [x7; xf(4)];
end

