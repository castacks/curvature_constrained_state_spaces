function xf = integrate_clothoid(x0, K1, K2, sf, delta_s)
%INTEGRATE_CLOTHOID Summary of this function goes here
%   Detailed explanation goes here

xf = zeros(size(x0));
s = linspace(0, sf, min(20, max(5, sf/delta_s)));
%s = linspace(0, sf, 5);
ds = s(2)-s(1);
psi = x0(3) + K1*s + ((K2-K1)/(2*(eps+sf)))*s.^2;

xf(1) = x0(1) + sum(cos(psi(1:(end-1)))*ds);
xf(2) = x0(2) + sum(sin(psi(1:(end-1)))*ds);
xf(3) = psi(end);
end

