function [ ineq_violations,eq_violations,gc,gceq ] = constraints( param, d, N, x0, xf, model, grad )
%CONSTRAINTS Summary of this function goes here
%   Detailed explanation goes here

[x_traj,u_traj,del_t] = unpack_parameters( param, d, N);

x_traj = [x0 x_traj xf];

delta_x = [model.V*del_t*cos(x_traj(3,1:(end-1))); model.V*del_t*sin(x_traj(3,1:(end-1))); del_t*x_traj(4,1:(end-1)); del_t*u_traj];


eq_violations=[reshape(diff(x_traj')' - delta_x, [], 1)];
% ineq_violations=[(x_traj(4,1:(end-1)) - model.psidot_max)';
%                  (- model.psidot_max - x_traj(4,1:(end-1)))';
%                  (u_traj - model.psiddot_max)';
%                  (-model.psiddot_max - u_traj)';
%                  -del_t];
ineq_violations=[];
gc = [];
gceq = sparse(grad(x_traj, u_traj, del_t)');

end

