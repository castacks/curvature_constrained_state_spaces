function length = path_length( param, d, N, x0, xf)
%PATH_LENGTH Summary of this function goes here
%   Detailed explanation goes here

[x_traj,~,~] = unpack_parameters( param, d, N);
x_traj = [x0 x_traj xf];
length = sum(sum(diff(x_traj(1:2,:)').^2,2));

end

