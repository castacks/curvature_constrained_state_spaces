function [  x_traj, u_traj, del_t ] = unpack_parameters( param, d, N)
%UNPACK_PARAMETERS Summary of this function goes here
%   Detailed explanation goes here

x_traj = reshape(param(1:(N*d)), d, []);
u_traj = param((N*d+1):(end-1));
del_t = param(end);
end

