%% This solves a BVP that minimizes length while satisfying nonlinear constraints
clc;
clear;
close all;

%% Boundary conditions
x0 = [0 0 0 0]';
xf = [2 2 pi 0]';
% FIXME: The cyclic angle is causing a problem - have to change the end
% point constraints
model.V = 1;
model.psidot_max = 2.5;
model.psiddot_max = 5*2.5;
model.r_min = model.V / model.psidot_max;
model.r_min_ub = model.r_min + 2 * model.V * (model.psidot_max / model.psiddot_max);

%% Initialize trajectory (w/0 dubin)
% N = 100;
% d = 4;
% x_traj = [linspace(x0(1), xf(1), N+2); linspace(x0(2), xf(2), N+2); linspace(x0(3), x0(3), N+2); linspace(x0(4), x0(4), N+2)];
% x_traj(:,1) = []; x_traj(:,2) = []; 
% u_traj = linspace(0, 0, N+1);
% del_t = 1/N;
% p0 = pack_parameters( x_traj, u_traj, del_t );


%% Initialize trajectory (dubin)
d = 4;
% FIXME: We can modify the bvp here based on unwrapping the dubin's psi
% solution
del_t = 0.2;
p_dubin = dubins(x0(1:3)', xf(1:3)', model.r_min, del_t*model.V);
psidot_traj = diff(p_dubin(3,:));
x_traj = p_dubin;
x_traj(:,1) = [];
x_traj = [x_traj; psidot_traj];
N = size(x_traj, 2);
u_traj = linspace(0, 0, N+1);
p0 = pack_parameters( x_traj, u_traj, del_t );



%% Symbollic derivative
x = sym('x', [4 N+2]);
x = sym(x, 'real');
u = sym('u', [1 N+1]);
u = sym(u, 'real');
dt = sym('dt', 'real');
delta_x = [model.V*dt*cos(x(3,1:(end-1))); model.V*dt*sin(x(3,1:(end-1))); dt*x(4,1:(end-1)); dt*u];
eq_violations=[reshape(x(:,2:end) - x(:,1:(end-1)) - delta_x, [], 1)];
p = pack_parameters( x(:,2:(end-1)), u, dt );
jacob = jacobian(eq_violations, p);
grad = matlabFunction(jacob, 'vars', {x, u, dt} );

%% Cost and constraints
cost_fn = @(x) path_length( x, d, N, x0, xf);
constraint_fn = @(x) constraints( x, d, N, x0, xf, model, grad);

lb_state = [min(x0(1), xf(1)) - 2*model.r_min_ub; min(x0(2), xf(2)) - 2*model.r_min_ub; -6*pi; -model.psidot_max];
ub_state = [max(x0(1), xf(1)) + 2*model.r_min_ub; max(x0(2), xf(2)) + 2*model.r_min_ub; 6*pi; model.psidot_max];

lb_traj = repmat(lb_state, 1, N);
lb_control = repmat(-model.psiddot_max, 1, N);
lb_time = 0;
ub_traj = repmat(ub_state, 1, N);
ub_control = repmat(model.psiddot_max, 1, N);
ub_time = 1; % get from dubin
lb = pack_parameters( lb_traj, lb_control, lb_time );
ub = pack_parameters( ub_traj, ub_control, ub_time );

%% Optimize
% set options for fmincon()
options = optimset('MaxFunEvals',1000000, 'TolFun', 1e-3,'MaxIter', 100, 'GradConstr','on','Display','iter','Algorithm', 'interior-point');

% do optimization
ticid = tic;
[p,fval,exitflag]=fmincon(cost_fn, p0, [],[],[],[],lb, ub, constraint_fn, options);
toc(ticid)

[  x_traj, u_traj, del_t ] = unpack_parameters( p, d, N);
x_traj = [x0 x_traj xf];
plot(x_traj(1,:), x_traj(2,:),'g');
hold on;
plot(p_dubin(1,:), p_dubin(2,:),'r');
hold off;
