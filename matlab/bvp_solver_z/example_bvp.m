%% This solves a BVP that minimizes length while satisfying nonlinear constraints
clc;
clear;
close all;

%% Boundary conditions

% FIXME: The cyclic angle is causing a problem - have to change the end
% point constraints
model.gamma_dot = 1/(30^2);
model.max_gamma = 5/30;

x0 = [100 0.1]'; 
xf = [0 0]'; 
s = 1200;
p0 = [0 0 0 0 (xf(1)-x0(1))/s];

%% Cost and constraints
cost_fn = @(x) cost_function( x );
constraints_fn = @(x) bvp_constraint( x , x0, xf, s);
lb = [0 0 -model.gamma_dot -model.gamma_dot -model.max_gamma];
ub = [s s model.gamma_dot model.gamma_dot model.max_gamma];

%% Optimize
% set options for fmincon()
options = optimset('MaxFunEvals',1000000, 'TolFun', 1e-3,'MaxIter', 100, 'Display','iter');

% do optimization
ticid = tic;
[p,fval,exitflag]=fmincon(cost_fn, p0, [], [], [], [], lb, ub, constraints_fn, options);
toc(ticid)

% plot
pt1 = linspace(0,p(1),100);
pt2 = linspace(0,p(2),100);
z1 = x0(1) + x0(2)*pt1 + 0.5*p(3)*pt1.^2;
z2 = x0(1) + x0(2)*p(1) + 0.5*p(3)*(p(1)^2) + p(5)*(s-p(1)-p(2)) + p(5)*pt2 + 0.5*p(4)*pt2.^2;
plot([pt1 s-p(2)+pt2], [z1 z2]);