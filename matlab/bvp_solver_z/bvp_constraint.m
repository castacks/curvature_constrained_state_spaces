function [ ineq, eq ] = bvp_constraint( p , x0, xf, s)
%BVP_CONSTRAINT Summary of this function goes here
%   Detailed explanation goes here


eq = [x0(2) + p(1)*p(3) - p(5);
      p(5) + p(2)*p(4) - xf(2);
      x0(2)*p(1) + 0.5*p(1)*p(1)*p(3) + p(5)*(s-p(1)-p(2)) + p(5)*p(2) + 0.5*p(2)*p(2)*p(4) - (xf(1) - x0(1))];
      
ineq = [p(1) + p(2) - s];
end
